
from run_cloth_cap_03223_soccerc_tilt_twist_left import ClothCapRunner_03223_soccerc_tilt_twist_left

need_to_find_A_mesh = False;
need_to_find_T_posed_mesh = True;
need_to_segment = False;
need_to_get_rings = False;
need_to_try_to_dress = False;
need_to_run_all_frames = False;




cloth_cap_runner = ClothCapRunner_03223_soccerc_tilt_twist_left()

if need_to_find_A_mesh:
    cloth_cap_runner.try_to_run();

if need_to_find_T_posed_mesh:
    cloth_cap_runner.compute_t_posed_mesh(current_smpl_path=r'E:\technion\computer_vision\final\final_project\output\\work_files_03223_soccerc_tilt_twist_left\mesh_smpl_T_posed.pkl');

if need_to_segment:
    cloth_cap_runner.assign_garments_labels();

if need_to_get_rings:
    cloth_cap_runner.collect_garment_rings();

if need_to_try_to_dress:
    cloth_cap_runner.try_to_dress();

if need_to_run_all_frames:
    cloth_cap_runner.run_all_frames();

