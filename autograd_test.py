import torch
from torch.autograd import Variable, grad

input1 = torch.tensor((1, 2, 3, 4));
input2 = torch.tensor((90, 92, 3, 40));

grad_test = torch.autograd.grad(inputs=torch.tensor((input1, input2)),
                                outputs=torch.tensor((5, 8)));

print(grad_test);