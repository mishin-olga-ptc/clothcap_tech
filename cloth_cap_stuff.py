import numpy as np
import torch
from torch import nn

from math_utils import pair_wise_squared_dist_matrix_torch, build_edges_matrix
from smpl_utils import get_A_pose_params


def E_pose_adopt(pose_params):
    A_pose_params = get_A_pose_params();

    diff = A_pose_params - torch.tensor(np.array(pose_params));
    squared_diff = diff * diff;

    result = squared_diff.sum();

    return result;


def E_shape_adopt(init_smpl_data, shape_params):

    diff = init_smpl_data.betas - shape_params;
    squared_diff = diff * diff;

    result = squared_diff.sum();

    return result;


def Eg_low(deformed_template_coords_tensor, scan_coords_tensor, weights = None, dim = 1):
    squared_distance_map = pair_wise_squared_dist_matrix_torch(scan_coords_tensor, deformed_template_coords_tensor);

    sigma = 0.5;

    squared_distances = squared_distance_map.min(dim);

    dists = squared_distances.values / (sigma + squared_distances.values);

    #if weights is not None:
     #   dists = dists * weights;

    actual_loss = torch.sum(dists) / dists.shape[0];

    result = actual_loss
    return result

def Eg(deformed_template_coords_tensor, scan_coords_tensor, weights = None):

    squared_distance_map = pair_wise_squared_dist_matrix_torch(scan_coords_tensor, deformed_template_coords_tensor);

    sigma = 0.5;

    squared_distances_0 = squared_distance_map.min(0);
    squared_distances_1 = squared_distance_map.min(1);

    dists_0 = squared_distances_0.values / (sigma + squared_distances_0.values);
    dists_1 = squared_distances_1.values / (sigma + squared_distances_1.values);

    #if weights is not None:
     #   dists = dists * weights;

    actual_loss_0 = torch.sum(dists_0) / dists_0.shape[0];
    actual_loss_1 = torch.sum(dists_1) / dists_1.shape[0];

    return (actual_loss_0 + actual_loss_1) / 2.0;



def Ec_adopt(deformed_template_coords,
             deformed_template_triangles,
             smpl_data, skin_weights):

    edges_matrix_1 = build_edges_matrix(deformed_template_coords,
                                        deformed_template_triangles, skin_weights);
    edges_matrix_2 = build_edges_matrix(torch.tensor(np.array(smpl_data.r)),
                                        torch.tensor(np.array(np.int64(smpl_data.f))),
                                        skin_weights);

    edges_diff = edges_matrix_1 - edges_matrix_2;

    vertex_indices_1 = deformed_template_triangles[:, 0];
    vertex_indices_2 = deformed_template_triangles[:, 1];
    vertex_indices_3 = deformed_template_triangles[:, 2];

    vertex_weights_1 = deformed_template_triangles[vertex_indices_1, 0].unsqueeze(dim=1);
    vertex_weights_2 = deformed_template_triangles[vertex_indices_2, 1].unsqueeze(dim=1);
    vertex_weights_3 = deformed_template_triangles[vertex_indices_3, 2].unsqueeze(dim=1);

    triangles_weights = (vertex_weights_1 + vertex_weights_2 + vertex_weights_3) / 3;

    weighted_distances = torch.pow(edges_diff, 2) * \
                         triangles_weights.repeat((1, edges_diff.shape[1]));

    return torch.sum(weighted_distances) / torch.sum(triangles_weights);

def smpl_part_loss(smpl_model, smpl_data_with_needed_shape, deformed_template_coords,
                   deformed_templates_triangles,
                   weights, w_c, w_pose, w_shape):

    loss_Ec = Ec_adopt(deformed_template_coords,
                       deformed_templates_triangles,
                       smpl_model, weights);

    if w_pose > 0:
        loss_E_pose = E_pose_adopt(smpl_model.pose);

    if w_shape > 0:
        loss_E_shape = E_shape_adopt(smpl_data_with_needed_shape, smpl_model.betas);

    loss = w_c * loss_Ec;
    if w_pose > 0:
        loss += w_pose * loss_E_pose;
    if w_shape > 0:
        loss_E_shape += w_shape * loss_E_shape;

    return loss;

def grad_fn_smpl(params, smpl_model, init_smpl, deformed_template_coords,
                 triangles_shape_tensor, weights, w_c, w_pose, w_shape):

    shape_params = params[0 : smpl_model.betas.shape[0]];
    pose_params = params[smpl_model.betas.shape[0] : ];

    smpl_data_shape_params_old = np.array(smpl_model.betas[:]);
    smpl_data_pose_params_old = np.array(smpl_model.pose[:]);

    smpl_model.betas[:] = shape_params[:];
    smpl_model.pose[:] = pose_params[:];

    loss = smpl_part_loss(smpl_model, init_smpl, deformed_template_coords,
                          triangles_shape_tensor,
                          weights, w_c, w_pose, w_shape);

    smpl_model.betas[:] = smpl_data_shape_params_old;
    smpl_model.pose[:] = smpl_data_pose_params_old;

    result = loss;

    return result;

def grad_fn_by_pose_smpl(pose_params, smpl_model, deformed_template_coords,
                         triangles_shape_tensor, weights, w_c, w_pose):

    smpl_data_pose_params_old = np.array(smpl_model.pose[:]);

    smpl_model.pose[:] = pose_params[:];

    loss = smpl_part_loss(smpl_model, None, deformed_template_coords,
                          triangles_shape_tensor,
                          weights, w_c, w_pose, 0.0);

    smpl_model.pose[:] = smpl_data_pose_params_old[:];

    result = loss;

    return result;


def grad_fn_Ec(params, smpl_model, deformed_template_coords,
               deformed_template_triangles, weights):

    shape_params = params[0 : smpl_model.betas.shape[0]];
    pose_params = params[smpl_model.betas.shape[0] : ];

    smpl_data_shape_params_old = np.array(smpl_model.betas[:]);
    smpl_data_pose_params_old = np.array(smpl_model.pose[:]);

    smpl_model.betas[:] = shape_params[:];
    smpl_model.pose[:] = pose_params[:];

    loss = Ec_adopt(deformed_template_coords, deformed_template_triangles, smpl_model, weights);

    smpl_model.betas[:] = smpl_data_shape_params_old;
    smpl_model.pose[:] = smpl_data_pose_params_old;

    result = loss;

    return result;

def grad_fn_Ec_shape_only(shape_params, smpl_model, deformed_template_coords, weights):

    smpl_data_shape_params_old = np.array(smpl_model.betas[:]);

    smpl_model.betas[:] = shape_params[:];

    loss = Ec_adopt(deformed_template_coords, smpl_model, weights);

    smpl_model.betas[:] = smpl_data_shape_params_old;

    result = loss;

    return result;

def smpl_distance_to_mesh_loss(deformed_template_coords, smpl_data):

    pdist = nn.PairwiseDistance(p=2);
    distances = pdist(deformed_template_coords, torch.tensor(np.array(smpl_data.r)));

    return distances;

def single_mesh_C_func(mesh_coords, posed_smpl_model, posed_coords):
    old_v_template = np.array(posed_smpl_model.v_template[:]);

    posed_smpl_model.v_template[:] = mesh_coords[:];

    distances = smpl_distance_to_mesh_loss(posed_coords, posed_smpl_model);

    result = torch.sum(distances);

    posed_smpl_model.v_template[:] = np.array(old_v_template);

    return result;


def single_mesh_C_grad_fn(mesh_coords, posed_smpl_model, posed_coords):

    N = posed_coords.shape[0];

    mesh_coords_to_use = np.reshape(mesh_coords, (N, 3));

    result = single_mesh_C_func(mesh_coords_to_use, posed_smpl_model, posed_coords);

    return result.item();

def E_v_adopt(mesh_garment_labels_weights, k_nearest_scan_labels_by_index_sum, cluster_weights,
              connection_cube, w_1, w_2, w_binary_term):

    scan_weighted_labels = mesh_garment_labels_weights * k_nearest_scan_labels_by_index_sum;

    first_term = - torch.log(scan_weighted_labels + 1.0);

    second_term = - torch.log(torch.pow((mesh_garment_labels_weights - cluster_weights), 2) + 1.0);

    unary_term = w_1 * first_term + w_2 * second_term.squeeze();

    vertex_count = unary_term.shape[0];


    label_rows = mesh_garment_labels_weights.unsqueeze(dim=1).repeat((1, vertex_count, 1));
    label_cols = mesh_garment_labels_weights.unsqueeze(dim=0).repeat((vertex_count, 1, 1));

    binary_term_matrix = torch.tensor(label_cols - label_rows).pow(2) * connection_cube;

    binary_term = torch.sum(binary_term_matrix, dim = 1);

    result = torch.sum(unary_term + w_binary_term * binary_term);

    return torch.sum(result);

def E_smooth(deformed_template_coords_tensor, rings):
    result = 0;
    garments_count = len(rings)
    for garments_rings in rings:
        garments_rings_size = len(garments_rings)
        current_result = 0;
        for ring in garments_rings:
            ring_size = len(ring)
            ring_coords = deformed_template_coords_tensor[ring, :];

            ring_indices_plus = np.roll(np.array(ring), 1)
            ring_indices_minus = np.roll(np.array(ring), -1)

            ring_coords_plus = deformed_template_coords_tensor[ring_indices_plus, :];
            ring_coords_minus = deformed_template_coords_tensor[ring_indices_minus, :];
            diff_from_average = ring_coords_plus + ring_coords_minus - 2 * ring_coords;

            current_result += torch.sum(torch.pow((diff_from_average), 2))/ ring_size;
        current_result = current_result / garments_rings_size;
        result += current_result;
    result = result / garments_count;
    return result;