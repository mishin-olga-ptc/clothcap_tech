import gc

from body_shape_weights import get_cloth_weight_matrix, get_garmet_weight
from math_utils import get_mass_center_from_indices, get_mass_center_with_weights
from plyfile import PlyData
from smpl_from_scan import smpl_from_scan_by_shape, smpl_from_scan_by_pose
from smpl_webuser.serialization import load_model, save_model
import numpy as np
from torch import nn
import torch
import sklearn.metrics as metrics
from autograd import grad
import scipy

def save_smpl_to_obj(smpl_data, file_path):
    ## Write to an .obj file
    outmesh_path = file_path
    with open(outmesh_path, 'w') as fp:
        for v in smpl_data.r:
            fp.write('v %f %f %f\n' % (v[0], v[1], v[2]))

        for f in smpl_data.f + 1:  # Faces are 1-based, not 0-based in obj files
            fp.write('f %d %d %d\n' % (f[0], f[1], f[2]))


def get_A_pose_params():
    A_pose_params = torch.zeros(72);
    A_pose_params[49] = -0.1;
    A_pose_params[50] = -1.2;
    A_pose_params[52] = 0.1;
    A_pose_params[53] = 1.2;
    A_pose_params[5] = 0.1;
    A_pose_params[8] = -0.1;

    return A_pose_params;



def debug_safe_smpl_with_mesh(smpl_model, mesh_coords, debug_folder_path, counter):
    N = smpl_model.r.shape[0];

    mesh_coords_to_use = np.reshape(mesh_coords, (N, 3));
    old_smpl_coords = np.array(smpl_model.v_template[:]);

    smpl_model.v_template[:] = mesh_coords_to_use[:];

    debug_obj_file_path = debug_folder_path + r'mesh_smpl_' + str(counter) + r'.obj';
    debug_smpl_file_path = debug_folder_path + r'mesh_smpl_' + str(counter) + r'.pkl';
    save_smpl_to_obj(smpl_model, debug_obj_file_path);
    save_model(smpl_model, debug_smpl_file_path);

    smpl_model.v_template[:] = old_smpl_coords[:];


def get_smpl_mass_center_with_weights(smpl_model, weights, threshold = 0):

    weights_to_use = np.zeros_like(weights);
    indices_to_use = np.where(weights > threshold);
    weights_to_use[indices_to_use] = weights[indices_to_use];

    result = get_mass_center_with_weights(torch.tensor(np.array(smpl_model.r)), weights_to_use);

    return result;

def get_smpl_funny_balet_pose():
    pose_params = torch.zeros(72);
    test = 2;

    pose_params[test * 3 + 0] = -1;
    pose_params[test * 3 + 1] = -1;
    pose_params[test * 3 + 2] = -1;

    test = 5;

    pose_params[test * 3 + 0] = 1.6;
    pose_params[test * 3 + 1] = 0;
    pose_params[test * 3 + 2] = 1;
    test = 7;
    pose_params[test * 3 + 0] = 1;
    pose_params[test * 3 + 1] = 0;
    pose_params[test * 3 + 2] = 0;
    test = 8;
    pose_params[test * 3 + 0] = 1;
    pose_params[test * 3 + 1] = 0;
    pose_params[test * 3 + 2] = 0;

    test = 11;
    pose_params[test * 3 + 0] = 1;
    pose_params[test * 3 + 1] = 0;
    pose_params[test * 3 + 2] = 0;

    test = 10;
    pose_params[test * 3 + 0] = 1;
    pose_params[test * 3 + 1] = 0;
    pose_params[test * 3 + 2] = 0;

    test = 18;

    pose_params[test * 3 + 0] = 0.3;
    pose_params[test * 3 + 1] = 0.3;
    pose_params[test * 3 + 2] = 0.3;
    test = 19;

    pose_params[test * 3 + 0] = -0.3;
    pose_params[test * 3 + 1] = -0.3;
    pose_params[test * 3 + 2] = -0.3;

    test = 21;

    pose_params[test * 3 + 0] = 0.3;
    pose_params[test * 3 + 1] = 0.3;
    pose_params[test * 3 + 2] = 0.3;
    test = 22;

    pose_params[test * 3 + 0] = -0.3;
    pose_params[test * 3 + 1] = -0.3;
    pose_params[test * 3 + 2] = -0.3;

    return pose_params;

def get_smpl_greetings_pose():
    A_pose_params = torch.zeros(72);

    A_pose_params[0] = -0.125;  # global orientation
    A_pose_params[1] = 0.0;
    A_pose_params[2] = 0.0;

    A_pose_params[3] = 0.0;  # left leg
    A_pose_params[4] = 0.2;
    A_pose_params[5] = 0.0;

    A_pose_params[6] = 0.2;  # right leg
    A_pose_params[7] = -0.2;
    A_pose_params[8] = -0.0;

    A_pose_params[9] = 0.125;  # low back
    A_pose_params[10] = 0.0;
    A_pose_params[11] = 0.0;

    A_pose_params[12] = 0.0;  # left knee
    A_pose_params[13] = 0.0;
    A_pose_params[14] = 0.0;
    A_pose_params[15] = 0.0;  # right knee
    A_pose_params[16] = 0.0;
    A_pose_params[17] = 0.0;

    A_pose_params[18] = 0.0;  # back
    A_pose_params[19] = 0.0;
    A_pose_params[20] = 0.0;

    A_pose_params[21] = 0.0;  # left ankle
    A_pose_params[22] = 0.0;
    A_pose_params[23] = 0.0;
    A_pose_params[24] = 0.0;  # right ankle
    A_pose_params[25] = 0.0;
    A_pose_params[26] = 0.0;

    A_pose_params[27] = 0.0;  # top back
    A_pose_params[28] = 0.0;
    A_pose_params[29] = 0.0;

    A_pose_params[30] = 0.0;  # lef foot
    A_pose_params[31] = 0.0;
    A_pose_params[32] = 0.0;
    A_pose_params[33] = 0.0;  # right foot
    A_pose_params[34] = 0.0;
    A_pose_params[35] = 0.0;

    A_pose_params[36] = 0.0;  # neck
    A_pose_params[37] = 0.0;
    A_pose_params[38] = 0.0;

    A_pose_params[39] = 0.0;  # left sholder
    A_pose_params[40] = 0.0;
    A_pose_params[41] = -0.3;

    A_pose_params[42] = 0.0;  # right sholder
    A_pose_params[43] = 0.0;
    A_pose_params[44] = 0.6;

    A_pose_params[45] = 0.0;  # neck
    A_pose_params[46] = 0.0;
    A_pose_params[47] = 0.0;

    A_pose_params[48] = 0.6;  # left arm
    A_pose_params[49] = -0.1;
    A_pose_params[50] = -0.15;
    A_pose_params[51] = 0.0;  # right arm
    A_pose_params[52] = -0.05;
    A_pose_params[53] = 0.49;

    A_pose_params[54] = 0.0;  # left cubit
    A_pose_params[55] = -0.8;
    A_pose_params[56] = 0.0;
    A_pose_params[57] = 0.0;  # right cubit
    A_pose_params[58] = 0.25;
    A_pose_params[59] = 0.0;

    A_pose_params[60] = 0.0;  # right hand
    A_pose_params[61] = 0.0;
    A_pose_params[62] = 0.3;
    A_pose_params[63] = 0.0;  # left hand
    A_pose_params[64] = 0.0;
    A_pose_params[65] = 0.0;

    A_pose_params[66] = 0.0;  # right fingers
    A_pose_params[67] = 0.0;
    A_pose_params[68] = 0.0;
    A_pose_params[69] = 0.0;  # left fingers
    A_pose_params[70] = 0.0;
    A_pose_params[71] = 0.0;

    return A_pose_params;

def zeroout_shape_info(smpl_data):
    pose_buf = np.array(smpl_data.pose[:]);
    smpl_data.pose[:] = 0;
    v_template_buff = np.array(smpl_data.r[:]);
    smpl_data.betas[:] = 0;
    smpl_data.v_template[:] = v_template_buff[:];
    smpl_data.pose[:] = pose_buf;
    return smpl_data;