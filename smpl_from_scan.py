from plyfile import PlyData
from smpl_webuser.serialization import load_model
import numpy as np
from torch import nn
import torch
import sklearn.metrics as metrics
from autograd import grad
import scipy

def loss_fn(smpl_coords_tensor, scan_coords_tensor):
    distance_map = metrics.pairwise.euclidean_distances(scan_coords_tensor, smpl_coords_tensor);

    sigma = 0.5;

    distances = distance_map.min(0);

    dists = distances / (sigma + distances);

    actual_loss = np.sum(dists, 0);

    result = actual_loss
    return result


def grad_fn_by_shape(shape_params, smpl_model, scan_coords):

    smpl_data_shape_params_old = np.array(smpl_model.betas[:]);

    smpl_model.betas[:] = shape_params[:];

    loss = loss_fn(smpl_model.r, scan_coords);

    smpl_model.betas[:] = smpl_data_shape_params_old;

    result = sum(shape_params) * 0 + loss;

    return result;

def grad_fn_by_pose(pose_params, smpl_model, scan_coords):

    smpl_data_pose_params_old = np.array(smpl_model.pose[:]);

    smpl_model.pose[:] = pose_params[:];

    loss = loss_fn(smpl_model.r, scan_coords);

    smpl_model.pose[:] = smpl_data_pose_params_old;

    result = sum(pose_params) * 0 + loss;

    return result;


def smpl_from_scan_by_shape(init_smpl_data, scan_coords_tensor, lr=0.0025, step=0.01):
    smpl_data = init_smpl_data;

    smpl_shape_array = np.array(smpl_data.betas);

    smpl_shape_array_old = torch.tensor(np.array(smpl_data.betas));

    loss = loss_fn(torch.tensor(smpl_data.r), scan_coords_tensor);
    prev_loss = loss;

    while loss <= prev_loss:
        grad = scipy.optimize.approx_fprime(smpl_shape_array,
                                            grad_fn_by_shape,
                                            step,
                                            smpl_data,
                                            scan_coords_tensor);
        smpl_data.betas[:] = np.array(smpl_data.betas) - np.array(grad * lr);
        prev_loss = loss;
        loss = loss_fn(torch.tensor(smpl_data.r), scan_coords_tensor);
        smpl_shape_array = np.array(smpl_data.betas);

    smpl_shape_array_new = torch.tensor(np.array(smpl_data.betas));
    diff = smpl_shape_array_old - smpl_shape_array_new;

    return smpl_data;


def smpl_from_scan_by_pose(init_smpl_data, scan_coords_tensor, lr=0.0025, step=0.01):
    smpl_data = init_smpl_data;

    smpl_pose_array = np.array(smpl_data.pose);

    smpl_pose_array_old = torch.tensor(np.array(smpl_data.pose));

    loss = loss_fn(torch.tensor(smpl_data.r), scan_coords_tensor);
    prev_loss = loss;

    while loss <= prev_loss:
        grad = scipy.optimize.approx_fprime(smpl_pose_array, grad_fn_by_pose, step, smpl_data, scan_coords_tensor);
        smpl_data.pose[:] = np.array(smpl_data.pose) - np.array(grad * lr);
        prev_loss = loss;
        loss = loss_fn(torch.tensor(smpl_data.r), scan_coords_tensor);
        smpl_pose_array = np.array(smpl_data.pose);

    smpl_pose_array_new = torch.tensor(np.array(smpl_data.pose));
    diff = smpl_pose_array_old - smpl_pose_array_new;

    return smpl_data;

