import gc

from body_shape_weights import get_cloth_weight_matrix
from plyfile import PlyData
from smpl_from_scan import smpl_from_scan_by_shape, smpl_from_scan_by_pose
from smpl_webuser.serialization import load_model
import numpy as np
from torch import nn
import torch
import sklearn.metrics as metrics
from autograd import grad
import scipy


def pair_wise_squared_dist_matrix_torch(points1_tensor, points2_tensor):
    N1 = points1_tensor.shape[0];
    N2 = points2_tensor.shape[0];
    D = 3;

    dot_prod = torch.matmul(points1_tensor, torch.t(points2_tensor));
    x1_tensor_square = points1_tensor * points1_tensor;
    x2_tensor_square = points2_tensor * points2_tensor;
    x1_tensor_square_sum = torch.sum(x1_tensor_square, 1);
    x2_tensor_square_sum = torch.sum(x2_tensor_square, 1);

    x1_tensor_square_sum_matrix = x1_tensor_square_sum.repeat(N2, 1);
    x2_tensor_square_sum_matrix = x2_tensor_square_sum.t().repeat(N1, 1).t();

    square_dists = (x1_tensor_square_sum_matrix + x2_tensor_square_sum_matrix - 2 * dot_prod.t()).t();

    return square_dists;

def get_height_scale_factor(scan_coords_tensor, smpl_coords, custom_height_factor = 1.0):
    max_scan_0 = torch.max(scan_coords_tensor[:, 0]);
    min_scan_0 = torch.min(scan_coords_tensor[:, 0]);
    mean_scan_0 = (max_scan_0 + min_scan_0) / 2.0;
    max_scan_1 = torch.max(scan_coords_tensor[:, 1]);
    min_scan_1 = torch.min(scan_coords_tensor[:, 1]);
    mean_scan_1 = (max_scan_1 + min_scan_1) / 2.0;
    max_scan_2 = torch.max(scan_coords_tensor[:, 2]);
    min_scan_2 = torch.min(scan_coords_tensor[:, 2]);
    mean_scan_2 = (max_scan_2 + min_scan_2) / 2.0;

    max_smpl_0 = torch.max(torch.tensor(np.array(smpl_coords[:, 0])));
    min_smpl_0 = torch.min(torch.tensor(np.array(smpl_coords[:, 0])));
    mean_smpl_0 = (max_scan_0 + min_scan_0) / 2.0;
    max_smpl_1 = torch.max(torch.tensor(np.array(smpl_coords[:, 1])));
    min_smpl_1 = torch.min(torch.tensor(np.array(smpl_coords[:, 1])));
    mean_smpl_1 = (max_smpl_1 + min_smpl_1) / 2.0;
    max_smpl_2 = torch.max(torch.tensor(np.array(smpl_coords[:, 2])));
    min_smpl_2 = torch.min(torch.tensor(np.array(smpl_coords[:, 2])));
    mean_smpl_2 = (max_scan_2 + min_scan_2) / 2.0;

    smpl_data_height = max_smpl_1 - min_smpl_1;
    scan_height = max_scan_1 - min_scan_1;

    height_scale_factor = 1.0 / scan_height * (smpl_data_height * custom_height_factor);

    return  height_scale_factor;

def get_move_mask(scan_coords_tensor, smpl_coords, backward_offset = 0.0, height_scale_factor = None):

    if height_scale_factor is not None:
        scan_coords_tensor = (scan_coords_tensor * height_scale_factor);

    min_scan_1 = torch.min(scan_coords_tensor[:, 1]);
    min_smpl_1 = torch.min(torch.tensor(np.array(smpl_coords[:, 1])));


    scan_average = torch.sum(scan_coords_tensor, dim=0) / scan_coords_tensor.shape[0];
    smpl_average = torch.sum(torch.tensor(np.array(smpl_coords)), dim=0) / smpl_coords.shape[0];
    move_mask = scan_average - smpl_average;
    move_mask[1] = min_scan_1 - min_smpl_1;
    move_mask[2] = move_mask[2] + backward_offset;
    return move_mask;

def scale_scan_coods(scan_coords_tensor, smpl_coords,
                     height_scale_factor = None,
                     custom_height_factor=1.0,
                     backward_offset = 0.0,
                     move_mask = None):

    if height_scale_factor is None:
        height_scale_factor = get_height_scale_factor(scan_coords_tensor, smpl_coords, custom_height_factor);

    scan_coords_tensor = (scan_coords_tensor * height_scale_factor);

    if move_mask is None:
        move_mask = get_move_mask(scan_coords_tensor, smpl_coords,
                                  backward_offset=backward_offset, height_scale_factor=None);

    move_mask_matrix = torch.tensor(move_mask).repeat((scan_coords_tensor.shape[0], 1));

    scan_coords_tensor = scan_coords_tensor - move_mask_matrix;

    return scan_coords_tensor;


def get_connection_matrix_from_traingles(vertex_count, traingles):
    vertices1 = traingles[:, 0];
    vertices2 = traingles[:, 1];
    vertices3 = traingles[:, 2];

    result = torch.zeros((vertex_count, vertex_count));

    result[vertices1, vertices2] = 1.0;
    result[vertices2, vertices1] = 1.0;

    result[vertices1, vertices3] = 1.0;
    result[vertices3, vertices1] = 1.0;


    result[vertices2, vertices3] = 1.0;
    result[vertices3, vertices2] = 1.0;

    return result;

def find_cycles(connection_matrix):
    vertex_count = connection_matrix.shape[0];

    rows = (torch.tensor(range(vertex_count)).unsqueeze(dim=1).repeat((1, vertex_count)));
    cols = (torch.tensor(range(vertex_count)).unsqueeze(dim=0).repeat((vertex_count, 1)));

    connection_matrix_copy = (rows >= cols).int() * connection_matrix;

    connection_matrix_copy = connection_matrix.clone().detach();


    rings = [];

    while(connection_matrix_copy.sum() > 0):

        mark_matrix = np.zeros(vertex_count);

        start_data = np.where(connection_matrix_copy != 0);
        start_vertex = start_data[0][0];

        stack = [start_vertex];

        ring = [];

        while len(stack) > 0:
            v = stack[-1];
            stack.remove(v);
            if mark_matrix[v] == 0:
                mark_matrix[v] = 1;
                ring.append(v);
                for j in range(len(start_data[0][:])):
                    if start_data[0][j] == v:
                        stack.append(start_data[1][j]);

        connection_matrix_copy[:, ring] = 0;
        connection_matrix_copy[ring, :] = 0;
        rings.append(ring);


    return rings;


def get_mass_center_with_weights(coords, weights):
    n = len(np.where(weights > 0)[0]);
    weighted_coords = coords * torch.tensor(np.tile(np.expand_dims(weights, axis=1), (1, 3)));

    result = torch.sum(weighted_coords, dim=0) / n;

    return result;

def get_mass_center_from_indices(coords, indices):

    n = indices.shape[0];
    needed_coords = torch.zeros_like(coords);
    needed_coords[indices, :] = coords[indices, :];

    result = torch.sum(needed_coords, dim=0) / n;

    return result;

def get_best_couples(distance_matrix):
    result = [];
    big_number = 10000;

    while torch.sum(big_number - distance_matrix) > 0:
        min_indices = np.unravel_index(np.argmin(distance_matrix), distance_matrix.shape);
        result.append(min_indices);
        distance_matrix[min_indices[0], :] = big_number;
        distance_matrix[:, min_indices[1]] = big_number;

    return result;

def build_edges_matrix(coords_tensor, triangles_tensor, vertex_weights):
    vertex_indices_1 = triangles_tensor[:, 0];
    vertex_indices_2 = triangles_tensor[:, 1];
    vertex_indices_3 = triangles_tensor[:, 2];

    vertices_1 = coords_tensor[vertex_indices_1];# * vertex_weights[vertex_indices_1].unsqueeze(dim = 1).repeat((1, 3));
    vertices_2 = coords_tensor[vertex_indices_2];# * vertex_weights[vertex_indices_1].unsqueeze(dim = 1).repeat((1, 3));
    vertices_3 = coords_tensor[vertex_indices_3];# * vertex_weights[vertex_indices_1].unsqueeze(dim = 1).repeat((1, 3));

    edge1 = torch.cat((vertices_1, vertices_2), 1);
    edge2 = torch.cat((vertices_2, vertices_3), 1);
    edge3 = torch.cat((vertices_3, vertices_1), 1);

    edges_matrix = torch.cat((edge1, edge2, edge3), 1);
    return edges_matrix;

def build_triangles_tensor(coords_tensor, triangles_tensor):
    vertex_indices_1 = triangles_tensor[:, 0];
    vertex_indices_2 = triangles_tensor[:, 1];
    vertex_indices_3 = triangles_tensor[:, 2];

    vertices_1 = coords_tensor[vertex_indices_1];
    vertices_2 = coords_tensor[vertex_indices_2];
    vertices_3 = coords_tensor[vertex_indices_3];

    result = torch.cat((vertices_1.unsqueeze(1),
                        vertices_2.unsqueeze(1),
                        vertices_3.unsqueeze(1)), dim=1);

    return result