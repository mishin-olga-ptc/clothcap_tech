
import run_first_step_tensor_flow
from run_cloth_cap_00005_shortlong_hips import ClothCapRunner_00005_shortlong_hips

need_to_save_scaled_scan_n_smpl = True;
need_to_find_A_mesh = True;
need_to_find_T_posed_mesh = True;
need_to_segment = True;
need_to_get_rings = True;
need_to_try_to_dress = True;
need_to_run_all_frames = True;

cloth_cap_runner = ClothCapRunner_00005_shortlong_hips()

if need_to_save_scaled_scan_n_smpl:
    cloth_cap_runner.save_smpl_n_scaled_scan()

if need_to_find_A_mesh:
    cloth_cap_runner.try_to_run();

if need_to_find_T_posed_mesh:
    cloth_cap_runner.compute_t_posed_mesh();

if need_to_segment:
    cloth_cap_runner.assign_garments_labels();

if need_to_get_rings:
    cloth_cap_runner.collect_garment_rings();

if need_to_try_to_dress:
    cloth_cap_runner.try_to_dress();

if need_to_run_all_frames:
    cloth_cap_runner.run_all_frames();



