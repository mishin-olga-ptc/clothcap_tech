import abc
import gc
import os

import numpy as np
import torch

from body_shape_weights import get_cloth_weight_matrix
from cloth_cap_stuff import smpl_part_loss, Eg, E_v_adopt, \
    E_smooth
from cloth_cap_utils import adjust_smpl_to_mesh, adjust_smpl_to_mesh_by_pose_shape, \
    get_k_nearest_points_from_scan
from math_utils import scale_scan_coods, get_connection_matrix_from_traingles, \
    get_height_scale_factor, get_move_mask
from ply_utils import set_scan_corrds_from_ply_data, get_scan_corrds_from_ply_data, debug_safe_ply, cluster_ply_scan, \
    save_ply_colored_by_weights, scale_n_save_ply_colored_by_clusters, \
    get_triangles_from_ply_data, get_colors_from_ply_data, split_ply_file_by_its_colors
from plyfile import PlyData
from smpl_utils import save_smpl_to_obj, get_A_pose_params, get_smpl_funny_balet_pose, get_smpl_greetings_pose, \
    zeroout_shape_info
from smpl_webuser.serialization import load_model, save_model


class ClothCapRunner(abc.ABC):

    def __init__(self):
        self.v = 1;
        self.height_scale_factor = None;
        self.init_folders();
        self.init_custom_folders()
        self.init_params();

    @abc.abstractmethod
    def init_params(self):
        return NotImplemented

    @abc.abstractmethod
    def init_params_for_second_step(self):
        return NotImplemented

    @abc.abstractmethod
    def init_custom_folders(self):
        return NotImplemented

    def init_folders(self):
        self.basic_smpl_folder_path = r'./SMPL_python_v.1.0.0/smpl/models/';
        self.basic_smpl_men_file_name = r'basicmodel_m_lbs_10_207_0_v1.0.0.pkl';
        self.basic_smpl_women_file_name = r'basicModel_f_lbs_10_207_0_v1.0.0.pkl';

    @abc.abstractmethod
    def init_custom_folders(self):
        return NotImplemented

    def save_smpl_n_scaled_scan(self):
        smpl_data = load_model(self.basic_smpl_file_path);
        first_frame_ply_data = PlyData.read(self.ply_fist_frame_file_path);
        t_pose_ply_data = PlyData.read(self.t_shape_mesh_file_path);
        smpl_data.pose[:] = self.get_adjusted_A_pose()[:];
        smpl_data_adjusted = self.init_smpl(smpl_data);

        scan_coords_tensor = get_scan_corrds_from_ply_data(first_frame_ply_data);

        scan_coords_tensor = self.scale_scan_corrds(scan_coords_tensor);

        scan_coords_tensor = scan_coords_tensor.float();

        save_model(smpl_data, self.output_folder + 'start_smpl_data_.pkl');
        save_smpl_to_obj(smpl_data, self.output_folder + 'start_smpl_data_.obj');

        debug_safe_ply(self.ply_current_frame_file_path,
                       scan_coords_tensor.clone().detach(),
                       self.output_folder + 'scan_coords_tensor_test.ply');



    def try_to_run(self,
                   init_smpl_path = None,
                   change_smpl_shape = True,
                   shape_adjusted_smpl_path=None,
                   start_deformed_template_path = None,
                   segmented_mesh_ply_path = None,
                   override_t_posed_smpl = False):

        smpl_data = load_model(self.basic_smpl_file_path);
        current_frame_ply_data = PlyData.read(self.ply_current_frame_file_path);
        first_frame_ply_data = PlyData.read(self.ply_fist_frame_file_path);
        t_pose_ply_data = PlyData.read(self.t_shape_mesh_file_path);

        old_smpl_data = smpl_data;

        smpl_data.pose[:] = self.get_adjusted_A_pose()[:];
        smpl_data_adjusted = self.init_smpl(smpl_data);

        if self.shape_adjusted_smpl_file_path is not None:
            save_model(smpl_data_adjusted, self.shape_adjusted_smpl_file_path);

        if init_smpl_path is not None:
            smpl_data = load_model(init_smpl_path);
        else:
            smpl_data = smpl_data_adjusted;
            smpl_data.pose[:] = self.get_adjusted_A_pose()[:];

        skin_weights = self.get_skin_weights_for_single_mesh(smpl_data) + 1;
        cloth_weights = get_cloth_weight_matrix(smpl_data);

        if start_deformed_template_path is not None:
            temp_ply_data = PlyData.read(start_deformed_template_path);
            deformed_template_coords = get_scan_corrds_from_ply_data(temp_ply_data);
            deformed_template_triangles = torch.tensor(get_triangles_from_ply_data(temp_ply_data)).long();

        else:
            deformed_template_coords = torch.tensor(smpl_data.r[:]);
            deformed_template_triangles = torch.tensor(np.int64(smpl_data.f[:]));

        scan_coords_tensor = get_scan_corrds_from_ply_data(current_frame_ply_data);

        deformed_template_coords = deformed_template_coords.float();

        if self.height_scale_factor is None:
            first_frame_scan_coords_tensor = get_scan_corrds_from_ply_data(first_frame_ply_data);
            self.height_scale_factor = get_height_scale_factor(first_frame_scan_coords_tensor, smpl_data.r,
                                                               self.custom_height_factor);

        scan_coords_tensor = self.scale_scan_corrds(scan_coords_tensor);

        counter = 0;
        scan_coords_tensor = scan_coords_tensor.float();

        if self.w_b > 0 or self.w_a > 0:
            self.make_and_save_segmented_scan_ply();
            rings = self.collect_garment_rings();
            mesh_border_coords_indices = self.get_coords_from_rings(rings);
            scan_border_coords = self.get_border_points_coods_from_scan().float();

        save_model(smpl_data, self.debug_folder + 'smpl_data_' + str(counter) + '.pkl');
        save_smpl_to_obj(smpl_data, self.output_folder + 'start_smpl_data_.obj');
        debug_safe_ply(self.ply_current_frame_file_path,
                       scan_coords_tensor.clone().detach(),
                       self.output_folder + 'scan_coords_tensor_' + str(counter) + '.ply');

        w_g = self.w_g;
        w_c = self.w_c;
        w_pose = self.w_pose;
        w_shape = self.w_shape;
        w_a = self.initial_w_a;
        w_b = self.w_b;
        do_adjust_smpl = True;

        def_temp_lr = self.def_temp_lr;
        momentum = self.momentum;
        threshold_stop_smpl_alignment =  self.threshold_stop_smpl_alignment
        threshold_zero_smpl_weight =  self.threshold_zero_smpl_weight
        threshold_zero_scan_to_mesh_weight = self.threshold_zero_scan_to_mesh_weight

        deformed_template_coords.requires_grad = True;
        loss_Eb = 0.0;
        loss_E_smooth = 0.0;
        loss_Eg = Eg(deformed_template_coords, scan_coords_tensor, cloth_weights);
        loss_smpl_part = smpl_part_loss(smpl_data, smpl_data_adjusted, deformed_template_coords,
                                        deformed_template_triangles,
                                        skin_weights, w_c, w_pose, w_shape);
        if w_b > 0:
            loss_Eb = Eg(deformed_template_coords[mesh_border_coords_indices, :], scan_border_coords, cloth_weights);

        if w_a > 0:
            loss_E_smooth = E_smooth(deformed_template_coords, rings);

        loss = w_g * loss_Eg + w_b * loss_Eb + w_a * loss_E_smooth + loss_smpl_part;

        if w_b > 0 or w_a > 0:
            print('loss_Eg = ', w_g * loss_Eg, ' loss_Eb = ', w_b * loss_Eb, ' loss_E_smooth =', w_a * loss_E_smooth,
                  ' loss_smpl_part = ', loss_smpl_part, ',  loss = ', loss, ' \n');
        else:
            print('loss_Eg = ', w_g * loss_Eg, ',  loss_smpl_part = ', loss_smpl_part, ',  loss = ', loss, ' \n');

        loss_prev = loss;

        loss.backward();
        grad = deformed_template_coords.grad;
        deformed_template_coords.grad = None;
        deformed_template_coords.requires_grad = False;

        deformed_template_coords_copy = torch.detach(deformed_template_coords);
        counter += 1;

        save_model(smpl_data, self.debug_folder + 'smpl_data_' + str(counter) + '.pkl');
        debug_safe_ply(self.t_pose_ply_file_path,
                       deformed_template_coords.clone().detach(),
                       self.debug_folder + 'deformed_template_coords_' + str(counter) + '.ply');
        counter += 1;

        keep_doing = True;
        params_changed = False;
        v = None;

        while keep_doing:
            gc.collect();

            deformed_template_coords_copy = torch.detach(deformed_template_coords);

            if v is None:
                v = torch.zeros_like(grad);

            v = momentum * v - (grad * def_temp_lr);
            deformed_template_coords_copy[:] = deformed_template_coords_copy[:] + v;

            deformed_template_coords_copy.requires_grad = True;

            loss_prev = loss.clone().detach();

            loss_Eg = Eg(deformed_template_coords_copy, scan_coords_tensor, cloth_weights);

            loss_Eb = 0.0;

            if w_b > 0:
                loss_Eb = Eg(deformed_template_coords[mesh_border_coords_indices, :], scan_coords_tensor, cloth_weights);

            if w_a > 0:
                loss_E_smooth = E_smooth(deformed_template_coords, rings);

            loss_smpl_part = smpl_part_loss(smpl_data,
                                            smpl_data_adjusted,
                                            deformed_template_coords_copy,
                                            deformed_template_triangles,
                                            skin_weights, w_c, w_pose, w_shape);

            loss = w_g * loss_Eg + w_b * loss_Eb + w_a * loss_E_smooth + loss_smpl_part;

            if w_b > 0 or w_a > 0:
                print('loss_Eg = ', w_g * loss_Eg,  ' loss_Eb = ', w_b * loss_Eb, ' loss_E_smooth =' , w_a * loss_E_smooth,
                      ' loss_smpl_part = ', loss_smpl_part, ',  loss = ', loss, ' \n');
            else:
                print('loss_Eg = ', w_g * loss_Eg, ',  loss_smpl_part = ', loss_smpl_part, ',  loss = ', loss, ' \n');

            loss.backward();
            grad = deformed_template_coords_copy.grad;

            grad[(torch.isnan(grad) == True).nonzero()] = 0.0;

            if do_adjust_smpl and (w_c > 0 or w_pose > 0 or w_shape > 0):
                smpl_data = adjust_smpl_to_mesh_by_pose_shape(smpl_data,
                                                              deformed_template_coords_copy.clone().detach(),
                                                              deformed_template_triangles,
                                                              smpl_data_adjusted,
                                                              self.step,
                                                              skin_weights, w_c, w_pose, w_shape,
                                                              self.smpl_adjust_threshold, self.smpl_adjust_max_iterations,
                                                              change_smpl_shape,
                                                              self.debug_folder,
                                                              r'smpl_data_iteration_' + str(counter) + r'_');

            debug_safe_ply(self.t_pose_ply_file_path,
                           deformed_template_coords_copy.clone().detach(),
                           self.debug_folder + 'deformed_template_coords_' + str(counter) + '.ply');
            save_model(smpl_data, self.debug_folder + 'smpl_data_' + str(counter) + '.pkl');
            counter += 1;


            if ((loss_prev.item() - loss.item()) < threshold_stop_smpl_alignment):
                do_adjust_smpl = False;
            if ((loss_prev.item() - loss.item()) < threshold_zero_smpl_weight):
                w_c = 0;
                w_pose = 0;
                w_shape = 0;

 #           if (w_b > 0 and (loss_prev.item() - loss.item()) < threshold_zero_scan_to_mesh_weight):
  #              w_g = 0;
   #             w_b = 0;
    #            w_a = self.w_a;
     #           def_temp_lr = self.smooth_ring_lr;

            keep_doing = ((loss.item() > 0.0 and
                           (loss_prev.item() - loss.item()) > self.smpl_n_mesh_threshold) or
                          counter < 3);



        set_scan_corrds_from_ply_data(t_pose_ply_data,
                                      deformed_template_coords_copy.clone().detach());

        self.save_frame_results(t_pose_ply_data, smpl_data);


    def save_frame_results(self, t_pose_ply_data, smpl_data):

        t_pose_ply_data.write(self.current_def_temp_file_path);

        save_smpl_to_obj(smpl_data, self.new_smpl_file_path);

        save_model(smpl_data, self.min_closed_smpl_file_path);

    def run_all_frames(self):

        self.init_params_for_second_step();

        current_smpl_file_path = self.mesh_smpl_T_posed;
        current_mesh_file_path = self.first_def_temp_file_path;
      #  self.shape_adjusted_smpl_file_path = None;
        self.output_folder = self.all_frames_output_path;

        counter = 0;

        for root, dirs, files in os.walk(self.data_frames_folder_path):
            for file in files:
                if counter > -1:
                    self.ply_current_frame_file_path = self.data_frames_folder_path + file;
                    self.current_def_temp_file_path = self.all_frames_output_path + r'def_temp_' + file;
                    self.new_smpl_file_path = self.all_frames_output_path + r'new_smpl_' + file + '.obj';
                    self.min_closed_smpl_file_path = self.all_frames_output_path + \
                                                     r'new_smpl_' + \
                                                     file.replace('.', '_') + '.pkl';
                    self.cloth_template_smpl_file_path = self.all_frames_output_path + \
                                                         r'cloth_template_' + \
                                                         file.replace('.', '_') + '.pkl';
                    self.cloth_template_obj_file_path = self.all_frames_output_path + \
                                                         r'cloth_template_' + \
                                                         file.replace('.', '_') + '.obj';

                    self.try_to_run(shape_adjusted_smpl_path=current_smpl_file_path,
                                    init_smpl_path=current_smpl_file_path,
                                    change_smpl_shape=False,
                                    start_deformed_template_path=current_mesh_file_path,
                                    segmented_mesh_ply_path = self.segmented_template_path);

                    self.cloth_template_smpl_file_path = self.all_frames_output_path + \
                                                         r'cloth_template_' + \
                                                         file.replace('.', '_') + '.pkl';
                    self.cloth_template_obj_file_path = self.all_frames_output_path + \
                                                        r'cloth_template_' + \
                                                        file.replace('.', '_') + '.obj'

                    self.compute_t_posed_mesh(output_smpl_path=self.cloth_template_smpl_file_path,
                                              output_obj_path=self.cloth_template_obj_file_path,
                                              current_smpl_path = current_smpl_file_path,
                                              smpl_for_pose_path = self.min_closed_smpl_file_path);

                    self.try_to_dress(ugly_smpl_path=self.cloth_template_smpl_file_path,
                                      take_pose_from_ugly_smpl=True,
                                      preffix=file.replace('.', '_'),
                                      funny_pose_segmented_garments_path = self.output_folder + r'funny_pose_segmented.ply');

                    self.try_to_dress(ugly_smpl_path=self.cloth_template_smpl_file_path,
                                      take_pose_from_ugly_smpl=True,
                                      shape=(-1.5 * np.ones(10)),
                                      preffix=file.replace('.', '_') + 'shape_',
                                      funny_pose_segmented_garments_path =
                                      self.output_folder + file.replace('.', '_') + 'shape_funny_pose_segmented.ply');


                    current_smpl_file_path = self.cloth_template_smpl_file_path;

                counter += 1;




    def compute_t_posed_mesh(self, output_smpl_path= None,
                             output_obj_path= None,
                             current_smpl_path = None,
                             smpl_for_pose_path = None):

        smpl_data_orig = load_model(self.basic_smpl_file_path);
        mesh_A_posed_ply_data = PlyData.read(self.current_def_temp_file_path);

        mesh_coords = get_scan_corrds_from_ply_data(mesh_A_posed_ply_data);


        if current_smpl_path is not None:
            smpl_data_orig = load_model(current_smpl_path);
        else:
            smpl_for_pose = load_model(self.shape_adjusted_smpl_file_path);
            smpl_data_orig.pose[:] = -np.array(smpl_for_pose.pose[:]);
            smpl_data_orig.betas[:] = -np.array(smpl_for_pose.betas[:]);
            smpl_data_orig.v_template[:] = mesh_coords.numpy()[:];
            v_template_buff = np.array(smpl_data_orig.r[:]);
            smpl_data_orig.pose[:] = 0;
            smpl_data_orig.betas[:] = 0;
            smpl_data_orig.v_template[:] = v_template_buff[:];
            smpl_data_orig.pose[:] = np.array(smpl_for_pose.pose[:]);
            smpl_data_orig.betas[:] = np.array(smpl_for_pose.betas[:]);

        if smpl_for_pose_path is not None:
            smpl_for_pose = load_model(smpl_for_pose_path);
            smpl_data_orig.pose[:] = np.array(smpl_for_pose.pose[:]);

        mesh_smpl_data = adjust_smpl_to_mesh(smpl_data_orig,
                                             mesh_coords,
                                             self.t_posed_mesh_gradient_step,
                                             self.t_posed_mesh_lr,
                                             self.t_posed_mesh_threshold,
                                             self.debug_folder);

        if output_obj_path is None:
            output_obj_path = self.mesh_obj_T_posed;
        if output_smpl_path is None:
            output_smpl_path = self.mesh_smpl_T_posed;


        save_model(mesh_smpl_data, output_smpl_path);
        save_smpl_to_obj(mesh_smpl_data, output_obj_path);

    def make_and_save_segmented_scan_ply(self, preffix = ''):
        self.segmented_scan_ply_data_path = self.output_folder + preffix + 'segmented_scan.ply';
        basic_smpl_model = load_model(self.basic_smpl_file_path);
        scan_ply_data = PlyData.read(self.ply_current_frame_file_path);
        clusters_indices = cluster_ply_scan(scan_ply_data, self.cluster_number);
        if self.height_scale_factor is None:
            self.height_scale_factor = get_height_scale_factor(get_scan_corrds_from_ply_data(scan_ply_data),
                                                          basic_smpl_model.r,
                                                          self.custom_height_factor);

        scaled_scan_coords = self.scale_scan_corrds(get_scan_corrds_from_ply_data(scan_ply_data));

        scale_n_save_ply_colored_by_clusters(self.ply_fist_frame_file_path, scan_ply_data,
                                             basic_smpl_model.r, scaled_scan_coords,
                                             clusters_indices, self.segmented_scan_ply_data_path,
                                             height_scale_factor=self.height_scale_factor,
                                             custom_height_factor=self.custom_height_factor,
                                             backward_offset=self.current_backward_offset);

    @abc.abstractmethod
    def get_initial_garment_weights(self):
        return NotImplemented

    def scale_scan_corrds(self, scan_coords):
        clean_smpl_model = load_model(self.shape_adjusted_smpl_file_path);
        first_scan_ply_data = PlyData.read(self.ply_fist_frame_file_path);
        first_frame_scan_coords_tensor = get_scan_corrds_from_ply_data(first_scan_ply_data);
        if self.height_scale_factor is None:
            self.height_scale_factor = get_height_scale_factor(first_frame_scan_coords_tensor, clean_smpl_model.r,
                                                               self.custom_height_factor);
        move_mask = get_move_mask(first_frame_scan_coords_tensor, clean_smpl_model.r,
                                  self.current_backward_offset, self.height_scale_factor);
        scaled_coords = scale_scan_coods(scan_coords, clean_smpl_model, self.height_scale_factor,
                                         self.custom_height_factor, self.current_backward_offset,
                                         move_mask);
        return scaled_coords;

    def assign_garments_labels(self):
        cloth_template_smpl = None;
        if os.path.exists(self.mesh_smpl_T_posed):
            cloth_template_smpl = load_model(self.mesh_smpl_T_posed);

        clean_smpl_model = load_model(self.shape_adjusted_smpl_file_path);
        mesh_ply_data = PlyData.read(self.first_def_temp_file_path);
        scan_ply_data = PlyData.read(self.ply_fist_frame_file_path);
        mesh_coords = get_scan_corrds_from_ply_data(mesh_ply_data);

        init_cluster_weights = self.get_initial_garment_weights();

        scaled_scan_coords = self.scale_scan_corrds(get_scan_corrds_from_ply_data(scan_ply_data));

        k_nearst_scan_indices = get_k_nearest_points_from_scan(mesh_ply_data, scaled_scan_coords, self.k);

        clusters_indices = cluster_ply_scan(scan_ply_data, self.cluster_number);

        if self.height_scale_factor is None:
            self.height_scale_factor = get_height_scale_factor(get_scan_corrds_from_ply_data(scan_ply_data),
                                                          clean_smpl_model.r,
                                                          self.custom_height_factor);

        scaled_scan_coords = self.scale_scan_corrds(get_scan_corrds_from_ply_data(scan_ply_data));

        scale_n_save_ply_colored_by_clusters(self.ply_fist_frame_file_path, scan_ply_data, mesh_coords.numpy(),
                                             scaled_scan_coords,
                                             clusters_indices, self.segmented_scan_path,
                                             height_scale_factor=self.height_scale_factor,
                                             custom_height_factor=self.custom_height_factor,
                                             backward_offset=self.backward_offset);

        k_neart_scan_labels = clusters_indices[k_nearst_scan_indices];

        index_cube = \
            torch.arange(self.cluster_number).unsqueeze(0).unsqueeze(0).repeat(k_neart_scan_labels.shape[0], self.k, 1);

        k_nearest_scan_labels_cube = \
            torch.tensor(k_neart_scan_labels).unsqueeze(2).repeat(1, 1, self.cluster_number);

        k_nearest_scan_labels_by_index_cube = (index_cube == k_nearest_scan_labels_cube).float();

        k_nearest_scan_labels_by_index_sum = torch.sum(k_nearest_scan_labels_by_index_cube, dim=1);

        k_nearest_scan_labels_by_index_scaled_sum = k_nearest_scan_labels_by_index_sum / self.k;

        cluster_weights = init_cluster_weights.clone().detach();

        cluster_weights.requires_grad = True;

        connection_matrix = get_connection_matrix_from_traingles(mesh_coords.shape[0],
                                                                 get_triangles_from_ply_data(mesh_ply_data));

        connection_cube = connection_matrix.unsqueeze(dim=2).repeat((1, 1, self.cluster_number));

        loss = E_v_adopt(cluster_weights, k_nearest_scan_labels_by_index_scaled_sum,
                         init_cluster_weights, connection_cube,
                         self.labels_w_1, self.labels_w_2, self.labels_binary_term);

        print('assign_garments_labels  loss = ', loss, ' \n');

        loss_prev = loss;
        counter = 0;

        loss.backward();
        grad = cluster_weights.grad;
        cluster_weights.grad = None;
        cluster_weights.requires_grad = False;

        mesh_garment_labels = torch.max(cluster_weights, dim=1).indices;
        save_ply_colored_by_weights(self.ply_fist_frame_file_path,
                                    mesh_ply_data, mesh_garment_labels.numpy(),
                                    self.debug_folder +  r'./labels_' + str(counter) + '.ply');

        while loss <= loss_prev:
            gc.collect();

            cluster_weights_copy = torch.detach(cluster_weights);

            cluster_weights_copy[:] = cluster_weights_copy[:] - \
                                               (grad * self.labels_lr);

            cluster_weights_copy.requires_grad = True;

            loss_prev = loss.clone().detach();
            loss = E_v_adopt(cluster_weights_copy, k_nearest_scan_labels_by_index_scaled_sum,
                             init_cluster_weights, connection_cube,
                             self.labels_w_1, self.labels_w_2, self.labels_binary_term);

            print('assign_garments_labels  loss = ', loss, ' \n');

            loss.backward();
            grad = cluster_weights_copy.grad;

            grad[(torch.isnan(grad) == True).nonzero()] = 0.0;

            counter += 1;
            mesh_garment_labels = torch.max(cluster_weights, dim=1).indices;
            save_ply_colored_by_weights(self.ply_fist_frame_file_path,
                                        mesh_ply_data, mesh_garment_labels.numpy(),
                                        self.debug_folder +  r'./labels_' + str(counter) + '.ply');

        save_ply_colored_by_weights(self.ply_fist_frame_file_path, mesh_ply_data,
                                    mesh_garment_labels.numpy(),
                                    self.segmented_template_path);

        split_ply_file_by_its_colors(self.segmented_template_path, self.output_folder, r'garment_');

        return  cluster_weights_copy;

    @abc.abstractmethod
    def collect_garment_rings(self,
                              segmented_ply_path,
                              segmented_border_template_path = None,
                              debug_folder = None):

        return NotImplemented

    def get_border_points_coods_from_scan(self):

        segemented_ply_data = PlyData.read(self.segmented_scan_ply_data_path);

        scan_coords = get_scan_corrds_from_ply_data(segemented_ply_data);

        scaled_scan_coords = self.scale_scan_corrds(scan_coords);

        segemented_ply_colors = get_colors_from_ply_data(segemented_ply_data);

        segmented_ply_triangles = get_triangles_from_ply_data(segemented_ply_data);

        vertex_count = segemented_ply_colors.shape[0];
        colors_as_numbers = segemented_ply_colors.long()[:, 0] * 255 * 255 + \
                            segemented_ply_colors.long()[:, 1] * 255 + \
                            segemented_ply_colors.long()[:, 2];

        vertices1 = segmented_ply_triangles[:, 0];
        vertices2 = segmented_ply_triangles[:, 1];
        vertices3 = segmented_ply_triangles[:, 2];

        colors1 = colors_as_numbers[vertices1];
        colors2 = colors_as_numbers[vertices2];
        colors3 = colors_as_numbers[vertices3];

        edges1 = colors1 != colors2;
        edges2 = colors2 != colors3;
        edges3 = colors3 != colors1;

        vertices1_indices1 = vertices1[np.where(edges1 == True)[0]];
        vertices1_indices2 = vertices2[np.where(edges2 == True)[0]];
        vertices2_indices1 = vertices2[np.where(edges2 == True)[0]];
        vertices2_indices2 = vertices3[np.where(edges3 == True)[0]];
        vertices3_indices1 = vertices3[np.where(edges3 == True)[0]];
        vertices3_indices2 = vertices1[np.where(edges1 == True)[0]];

        indices = np.unique(np.concatenate((np.unique(vertices1_indices1),
                                            np.unique(vertices1_indices2),
                                            np.unique(vertices2_indices1),
                                            np.unique(vertices2_indices2),
                                            np.unique(vertices3_indices1),
                                            np.unique(vertices3_indices2))));

        result = scaled_scan_coords[indices, :];
        return result;


    def get_coords_from_rings(self, rings):
        result = None;
        for garment_rings in rings:
            for ring in garment_rings:
                if result is None:
                    result = np.array(ring);
                else:
                    result = np.concatenate((result, np.array(ring)));

        result = np.unique(result);


        return result;

    @abc.abstractmethod
    def adjust_nacked_smpl_to_dress(self, smpl_data):

        return smpl_data;

    @abc.abstractmethod
    def adjust_cloth_smpl_to_dress(self, smpl_data):

        return smpl_data;

    def try_to_dress(self, pose = None, shape = None,
                     ugly_smpl_path = None,
                     take_pose_from_ugly_smpl = False,
                     preffix = '', funny_pose_segmented_garments_path = None):

        nacked_smpl = load_model(self.shape_adjusted_smpl_file_path);

        if ugly_smpl_path is not None:
            ugly_smpl = load_model(ugly_smpl_path);
        else:
            ugly_smpl = load_model(self.mesh_smpl_T_posed);

        if pose is None:
            if take_pose_from_ugly_smpl:
                pose = np.array(ugly_smpl.pose[:]);
            else:
                pose = get_smpl_greetings_pose();


        if np.sum(np.array(ugly_smpl.betas[:] != nacked_smpl.betas[:])) > 0:
            ugly_smpl = zeroout_shape_info(ugly_smpl);
            nacked_smpl = zeroout_shape_info(nacked_smpl);

        nacked_smpl = self.adjust_nacked_smpl_to_dress(nacked_smpl);
        ugly_smpl = self.adjust_cloth_smpl_to_dress(ugly_smpl);

        if shape is not None:
            nacked_smpl.betas[:] = shape[:];
        if shape is not None:
            ugly_smpl.betas[:] = shape[:];

        if pose is None:
            pose = get_smpl_funny_balet_pose();

        nacked_smpl.pose[:] = pose[:];
        ugly_smpl.pose[:] = pose[:];

        colored_ply = PlyData.read(self.segmented_template_path);
        colors = get_colors_from_ply_data(colored_ply);

        output_ply_data = set_scan_corrds_from_ply_data(colored_ply,
                                                        torch.tensor(np.array(ugly_smpl.r)));

        if funny_pose_segmented_garments_path is None:
            funny_pose_segmented_garments_path = self.funny_pose_segmented_garments_path;

        output_ply_data.write(funny_pose_segmented_garments_path);

        split_ply_file_by_its_colors(funny_pose_segmented_garments_path,
                                     self.output_folder,
                                     preffix + 'funny_pose_garments_')

        save_smpl_to_obj(nacked_smpl, self.output_folder + preffix + 'nacked.obj');

    @abc.abstractclassmethod
    def demo(self):
        return 0

    @abc.abstractclassmethod
    def init_smpl(self, smpl_model):
        return smpl_model

    @abc.abstractclassmethod
    def get_skin_weights_for_single_mesh(self, smpl_model):
        return None;

    @abc.abstractclassmethod
    def get_adjusted_A_pose(self):
        return get_A_pose_params();