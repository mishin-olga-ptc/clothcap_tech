import numpy as np
import torch

from body_shape_weights import get_garmet_weight, get_no_cloth_weights, get_women_skin_without_socks_weight_matrix
from cloth_cap_utils import get_garment_rings, save_garment_ring_to_ply_file
from ply_utils import save_ply_colored_by_weights, get_scan_corrds_from_ply_data
from plyfile import PlyData
from run_first_step_tensor_flow import ClothCapRunner
from serialization import load_model
from smpl_utils import get_smpl_greetings_pose


class ClothCapRunner_03223_soccerc_tilt_twist_left(ClothCapRunner):

    def init_params(self):

        self.smpl_adjust_threshold = 0.001; #threshold for adjusting SMPL to the mesh while computing single mesh alignment
        self.smpl_adjust_max_iterations = 20; # maximal number of iterations of smpl adjustment whild computing
        #single mesh alignment

        self.def_temp_lr = 0.001; # learning rate for the coordinates while computing single mesh alignment
        self.smooth_ring_lr = 0.00001; # learning rate for the coordinates while trying to make the grament border smooth
        self.momentum = 0.5 # momentum (considering the previous gradient) while computing single mesh alignment
        self.threshold_stop_smpl_alignment = 1.0 # after the previous and the current loss function differes no more
        #than by this value, we stop SMPL alignment
        self.threshold_zero_smpl_weight = 0.5; #after the previous and the current loss function differes no more
        #than by this value, we set the weights for the shape and pose alignment to zero
        self.step = 0.00001; #gradient step for SMPL alignment while adjusting SMPL to the mesh

        self.w_c = 396; #computing single mesh alignment: weight Wc exactly as in the paper
        self.w_g = 97600;#computing single mesh alignment: weight Wg exactly as in the paper
        self.w_pose = 3;#computing single mesh alignment: weight for the pose
        self.w_shape = 1;#computing single mesh alignment: weight for the shape

        self.t_posed_mesh_gradient_step = 0.0001;#computing the cloth template: gradient step
        self.t_posed_mesh_lr = 0.025;#computing the cloth template: learning rate. In the current implementation not used
        self.t_posed_mesh_threshold = 0.001;#computing the cloth template: threshold, after the function loss improves
        #less than this threshold, the algorithm stops and returns the result

        self.labels_lr = 0.1;#computing segmentation: learning rate
        self.labels_w_1 = 0.1;#computing segmentation: weight of the first term of the unary term
        self.labels_w_2 = 0.1;#computing segmentation: weight of the second term of the unary term
        self.labels_binary_term = 0.1;#computing segmentation: weight of the binary term

        self.smpl_n_mesh_threshold = 0.001; #computing single mesh alignment, SMPL slignment, the treshold after
        #the function loss improves less than this thrishold, we stop the alignment

        self.cluster_number = 3; #number of clusters for segmentation: number of garments + 1
        self.k = 6; #k nearest points in the scan for segmentation
        self.w_b = 0.0;#weights for the border lines - distances from the scan
        self.initial_w_a = 0.0;#weights for the border lines - smoothness - in the beggining
        self.w_a = 0.0;#weights for the border lines - smoothness - in the end
        self.threshold_zero_scan_to_mesh_weight = 0.1;#in the second step, while computing single mesh alignment
        #for each frame, the loss diffrence after which we consider the border smoothness only (not used)

        self.custom_height_factor = 1.135;#the factor for the scaling of the scan. Needed because of very noisy input
        self.init_first_frame_backward_offset =  0.02125;#the distance we "move" the scan backward in the alignment.
        self.current_backward_offset = self.init_first_frame_backward_offset;

    def init_params_for_second_step(self):
        self.init_params();
        self.w_pose = 0.0;
        self.w_shape = 0.0;
        self.w_c = 39.6;
        self.w_b = 60;
        self.initial_w_a = 3920.0;
        self.w_a = 3920;
        self.w_g = 5760;
        self.momentum = 0.05;
        self.def_temp_lr = 0.000001;
        self.current_backward_offset = self.init_first_frame_backward_offset;




    def init_custom_folders(self):

        super().init_folders();

        self.data_frames_folder_path = r'../data/03223/03223/soccerc_tilt_twist_left/';#input data frames path
        self.t_shape_mesh_file_path = r'../data/gt_shapes/gt_shapes/03223_minimal_all_frames.ply';#input data minimal clothed shape
        self.t_pose_ply_file_path = self.t_shape_mesh_file_path;
        self.debug_folder_path = r'./output/debug_03223_soccerc_tilt_twist_left/';#output folder pash for debug files
        self.debug_folder = self.debug_folder_path;
        self.work_folder_path = r'./output/work_files_03223_soccerc_tilt_twist_left/';#output folder path for real output and next steps
        self.output_folder = self.work_folder_path;

        self.ply_T_pose_file_path = self.t_shape_mesh_file_path;
        self.ply_fist_frame_file_path = self.data_frames_folder_path + r'soccerc_tilt_twist_left.000001.ply';#the path to the first frame
        self.basic_smpl_file_path = self.basic_smpl_folder_path + self.basic_smpl_women_file_name;#basic SMPL file path
        self.ply_current_frame_file_path = self.ply_fist_frame_file_path;

        self.first_def_temp_file_path = self.work_folder_path + 'deformed_template.ply';#output - single mesh alignment
        self.current_def_temp_file_path = self.first_def_temp_file_path;
        self.def_temp_no_dirty_trick_file_path = self.work_folder_path + 'deformed_template_no_dirty_trick.ply';
        #output -

        self.new_smpl_file_path = self.work_folder_path + 'min_clothed_shape.obj';#the adjusted SMPL after
        # single mesh alignment computing (obj file to view)
        self.min_closed_smpl_file_path = self.work_folder_path + 'min_clothed_smpl.pkl';#the adjusted SMPL after
        # single mesh alignment computing
        self.shape_adjusted_smpl_file_path = self.work_folder_path + 'smpl_adjusted_to_T_pose_given.pkl'; #start SMPL
        #path - the manually aligned before the algorithm

        self.mesh_smpl_T_posed = self.work_folder_path + r'mesh_smpl_T_posed.pkl';#clothed template SMPL
        self.mesh_obj_T_posed = self.work_folder_path + r'mesh_smpl_T_posed.obj';#clothed template SMPL obj file
        self.segmented_template_path = self.work_folder_path + r'./segmented_template.ply';#the single mesh alignment
        #colored by segmentation
        self.segmented_scan_path = self.work_folder_path + r'./scan_clusters.ply';#the input frame colored by segmentation
        self.all_frames_output_path = self.work_folder_path + r'all frames/';#all frames output folder path
        self.segmented_border_template_path = self.work_folder_path + r'./segmented_template_with_borders.ply';#the single mesh alignment
        #colored by segmentation with borders

        self.funny_pose_segmented_garments_path = self.work_folder_path + r'funny_pose_segmented.ply';#output file path for a small demo


    def get_initial_garment_weights(self):
        long_socks_key = 'long_socks';
        tshirt_key = 'tshirt';
        shorts_key = 'shorts';

        clean_smpl_model = load_model(self.shape_adjusted_smpl_file_path);
        mesh_ply_data = PlyData.read(self.first_def_temp_file_path);

        t_shirt_weights = get_garmet_weight(clean_smpl_model, tshirt_key);
        shorts_weights = get_garmet_weight(clean_smpl_model, shorts_key);
        long_socks_weights = get_garmet_weight(clean_smpl_model, long_socks_key);

        t_shirt_n_socks_weights = (t_shirt_weights + long_socks_weights) / 2.0;

        skin_weights = get_women_skin_without_socks_weight_matrix(clean_smpl_model);
        #skin_weights = get_no_cloth_weights(clean_smpl_model, [tshirt_key, long_socks_key, shorts_key]);

        save_ply_colored_by_weights(self.ply_fist_frame_file_path, mesh_ply_data, t_shirt_n_socks_weights,
                                    self.output_folder +  r'./prior_' + 't_shirt_n_socks' + '.ply');
        save_ply_colored_by_weights(self.ply_fist_frame_file_path, mesh_ply_data, shorts_weights,
                                    self.output_folder +  r'./prior_' + shorts_key + '.ply');
        save_ply_colored_by_weights(self.ply_fist_frame_file_path, mesh_ply_data, skin_weights.numpy(),
                                    self.output_folder +  r'./prior_' + 'skin' + '.ply');

        init_cluster_weights = torch.tensor(([shorts_weights,
                                              t_shirt_n_socks_weights,
                                              skin_weights.numpy()])).t();
        return init_cluster_weights;


    def collect_garment_rings(self):

        rings = get_garment_rings(self.segmented_template_path,
                                  self.segmented_border_template_path,
                                  self.debug_folder_path, r'garment_ring_');


        real_rings = [];
        t_thirt_rings = rings[2];
        t_thirt_rings_sorted = sorted(t_thirt_rings, key=len, reverse=True);
        real_t_thirt_rings = t_thirt_rings_sorted[0:4];

        shorts_rings = rings[1];
        shorts_rings_sorted = sorted(shorts_rings, key=len, reverse=True);
        real_shorts_rings = shorts_rings_sorted[0:4];

        del real_shorts_rings[1];

        real_rings.append(real_shorts_rings);
        real_rings.append(real_t_thirt_rings);

        counter  = 0;

        for garment_rings in real_rings:

            save_garment_ring_to_ply_file(counter, r'garment_rings_', self.work_folder_path, garment_rings,
                                          self.segmented_template_path);
            counter += 1;

        return real_rings;



    def adjust_nacked_smpl_to_dress(self, smpl_data):


        return smpl_data;

    def adjust_cloth_smpl_to_dress(self, smpl_data):

        return smpl_data;

    def init_smpl(self, smpl_model=None):

        t_pose_ply_data = PlyData.read(self.t_shape_mesh_file_path)
        mesh_coords = get_scan_corrds_from_ply_data(t_pose_ply_data);

        if smpl_model is None:
            smpl_model = load_model(self.basic_smpl_file_path);

        smpl_model.v_template[:] = mesh_coords.numpy();
        smpl_model.betas[0] = -1.5;
        smpl_model.betas[1] = 0.5;
        smpl_model.betas[4] = 5;
        return smpl_model;

    def get_skin_weights_for_single_mesh(self, smpl_model):
        return get_no_cloth_weights(smpl_model, ['tshirt', 'shorts']);

    def get_adjusted_A_pose(self):
        A_pose_params = torch.zeros(72);

        A_pose_params[0] = 0.025;# global orientation
        A_pose_params[1] = 0.0;
        A_pose_params[2] = 0.0;

        A_pose_params[3] = 0.025;  # left leg
        A_pose_params[4] = 0.0;
        A_pose_params[5] = 0.1;

        A_pose_params[6] = 0.025;  # right leg
        A_pose_params[7] = -0.0;
        A_pose_params[8] = -0.1;

        A_pose_params[9] =  0.07; # low back
        A_pose_params[10] = 0.0;
        A_pose_params[11] = 0.0;

        A_pose_params[12] = -0.15;# left knee
        A_pose_params[13] = 0.0;
        A_pose_params[14] = 0.0;
        A_pose_params[15] = -0.15;# right knee
        A_pose_params[16] = 0.0;
        A_pose_params[17] = 0.0;

        A_pose_params[18] = -0.0125; # back
        A_pose_params[19] = 0.0;
        A_pose_params[20] = 0.0;

        A_pose_params[21] = 0.0; # left ankle
        A_pose_params[22] = 0.0;
        A_pose_params[23] = 0.0;
        A_pose_params[24] = 0.0;  # right ankle
        A_pose_params[25] = 0.0;
        A_pose_params[26] = 0.0;

        A_pose_params[27] = -0.15;  # top back
        A_pose_params[28] = 0.0;
        A_pose_params[29] = 0.0;

        A_pose_params[30] = 0.0; # lef foot
        A_pose_params[31] = 0.0;
        A_pose_params[32] = 0.0;
        A_pose_params[33] = 0.0;  # right foot
        A_pose_params[34] = 0.0;
        A_pose_params[35] = 0.0;

        A_pose_params[36] = -0.1;  # neck
        A_pose_params[37] = 0.0;
        A_pose_params[38] = 0.0;

        A_pose_params[39] = -0.25;  # left sholder
        A_pose_params[40] = 0.0;
        A_pose_params[41] = -0.4;

        A_pose_params[42] = -0.25;  # right sholder
        A_pose_params[43] = 0.0;
        A_pose_params[44] = 0.4;

        A_pose_params[45] = -0.05;  #neck
        A_pose_params[46] = 0.0;
        A_pose_params[47] = 0.0;

        A_pose_params[48] = 0.25;  # left arm
        A_pose_params[49] = 0.25;
        A_pose_params[50] = -0.8;
        A_pose_params[51] = 0.25;  # right arm
        A_pose_params[52] = -0.15;
        A_pose_params[53] = 0.8;

        A_pose_params[54] = 0.0;  # left cubit
        A_pose_params[55] = -0.2;
        A_pose_params[56] = 0.0;
        A_pose_params[57] = 0.0;  # right cubit
        A_pose_params[58] = 0.1;
        A_pose_params[59] = 0.0;

        A_pose_params[60] = 0.0;  # right hand
        A_pose_params[61] = 0.0;
        A_pose_params[62] = 0.0;
        A_pose_params[63] = 0.0;  # left hand
        A_pose_params[64] = 0.0;
        A_pose_params[65] = 0.0;

        A_pose_params[66] = 0.0;  # right fingers
        A_pose_params[67] = 0.0;
        A_pose_params[68] = 0.0;
        A_pose_params[69] = 0.0;  # left fingers
        A_pose_params[70] = 0.0;
        A_pose_params[71] = 0.0;

        return A_pose_params;

    def demo(self):
        shape = -np.ones(10);
        self.try_to_dress(pose = get_smpl_greetings_pose(), shape=shape);
        return 0
