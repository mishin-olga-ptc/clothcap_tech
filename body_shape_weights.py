import gc

from plyfile import PlyData
from smpl_from_scan import smpl_from_scan_by_shape, smpl_from_scan_by_pose
from smpl_webuser.serialization import load_model
import numpy as np
from torch import nn
import torch
import sklearn.metrics as metrics
from autograd import grad
import scipy

def joints_dict():
    result = {
        "global_orientation": 0,
        "lef_leg": 1,
        "right_leg": 2,
        "low_back": 3,
        "left_knee": 4,
        "right_knee": 5,
        "back": 6,
        "left_ankle": 7,
        "right_ankle": 8,
        "upper_back": 9,
        "left_foot": 10,
        "right_foot": 11,
        "neck_low": 12,
        "left_arm_sholder_upper": 13,
        "right_arm_sholder_upper": 14,
        "neck_high": 15,
        "left_arm_sholder_low": 16,
        "right_arm_sholder_low": 17,
        "left_cubit": 18,
        "right_cubit": 19,
        "left_hand": 20,
        "right_hand": 21,
        "left_hand_fingers": 22,
        "right_hand_fingers": 23
    };
    return result;

def garmets_dict():

    result = {
        "tshirt": {"low_back" : 1.0,
                   "back" : 1.0,
                   "upper_back" : 1.0,
                   "neck_low" : 0.5,
                   "left_arm_sholder_upper" : 1.0,
                   "right_arm_sholder_upper" : 1.0,
                   "left_arm_sholder_low" : 1.0,
                   "right_arm_sholder_low" : 1.0},
        "jeans": {"lef_leg" : 1.0,
                  "right_leg" : 1.0,
                  "low_back" : 1.0,
                  "left_knee" : 1.0,
                  "right_knee" : 1.0,
                  "left_ankle" : 0.5,
                  "right_ankle" : 0.5},
        "socks": {"left_foot": 1.0,
                  "right_foot": 1.0,
                  "left_ankle": 0.5,
                  "right_ankle": 0.5}
    };
    return result;

def not_garmets_dict():

    result = {
        "tshirt": {"global_orientation" : 0.0,
                   "lef_leg" : 1.0,
                   "right_leg" : 1.0,
                   "low_back": 0.0,
                   "left_knee" : 1.0,
                   "right_knee" : 1.0,
                   "back": 0.0,
                   "left_ankle" : 1.0,
                   "right_ankle" : 1.0,
                   "upper_back": 0.0,
                   "left_foot" : 1.0,
                   "right_foot" : 1.0,
                   "neck_low": 0.5,
                   "left_arm_sholder_upper" : 0.0,
                   "right_arm_sholder_upper" : 0.0,
                   "neck_high": 1.0,
                   "left_arm_sholder_low" : 0.0,
                   "right_arm_sholder_low" : 0.0,
                   "left_cubit" : 1.0,
                   "right_cubit" : 1.0,
                   "left_hand" : 1.0,
                   "right_hand" : 1.0,
                   "left_hand_fingers" : 1.0,
                   "right_hand_fingers" : 1.0},
        "jeans": {"global_orientation" : 0.0,
                   "lef_leg" : 0.0,
                   "right_leg" : 0.0,
                   "low_back": 0.0,
                   "left_knee" : 0.0,
                   "right_knee" : 0.0,
                   "back": 1.0,
                   "left_ankle" : 0.5,
                   "right_ankle" : 0.5,
                   "upper_back": 1.0,
                   "left_foot" : 1.0,
                   "right_foot" : 1.0,
                   "neck_low": 1.0,
                   "left_arm_sholder_upper" : 1.0,
                   "right_arm_sholder_upper" : 1.0,
                   "neck_high": 1.0,
                   "left_arm_sholder_low" : 1.0,
                   "right_arm_sholder_low" : 1.0,
                   "left_cubit" : 1.0,
                   "right_cubit" : 1.0,
                   "left_hand" : 1.0,
                   "right_hand" : 1.0,
                   "left_hand_fingers" : 1.0,
                   "right_hand_fingers" : 1.0},
        "shorts": {"global_orientation" : 0.0,
                   "lef_leg" : 0.0,
                   "right_leg" : 0.0,
                   "low_back": 0.0,
                   "left_knee" : 0.5,
                   "right_knee" : 0.5,
                   "back": 1.0,
                   "left_ankle" : 1.0,
                   "right_ankle" : 1.0,
                   "upper_back": 1.0,
                   "left_foot" : 1.0,
                   "right_foot" : 1.0,
                   "neck_low": 1.0,
                   "left_arm_sholder_upper" : 1.0,
                   "right_arm_sholder_upper" : 1.0,
                   "neck_high": 1.0,
                   "left_arm_sholder_low" : 1.0,
                   "right_arm_sholder_low" : 1.0,
                   "left_cubit" : 1.0,
                   "right_cubit" : 1.0,
                   "left_hand" : 1.0,
                   "right_hand" : 1.0,
                   "left_hand_fingers" : 1.0,
                   "right_hand_fingers" : 1.0},
        "socks": {"global_orientation" : 1.0,
                   "lef_leg" : 1.0,
                   "right_leg" : 1.0,
                   "low_back": 1.0,
                   "left_knee" : 1.0,
                   "right_knee" : 1.0,
                   "back": 1.0,
                   "left_ankle" : 0.0,
                   "right_ankle" : 0.0,
                   "upper_back": 1.0,
                   "left_foot" : 0.0,
                   "right_foot" : 0.0,
                   "neck_low": 1.0,
                   "left_arm_sholder_upper" : 1.0,
                   "right_arm_sholder_upper" : 1.0,
                   "neck_high": 1.0,
                   "left_arm_sholder_low" : 1.0,
                   "right_arm_sholder_low" : 1.0,
                   "left_cubit" : 1.0,
                   "right_cubit" : 1.0,
                   "left_hand" : 1.0,
                   "right_hand" : 1.0,
                   "left_hand_fingers" : 1.0,
                   "right_hand_fingers" : 1.0},
        "long_socks": {"global_orientation": 1.0,
                  "lef_leg": 1.0,
                  "right_leg": 1.0,
                  "low_back": 1.0,
                  "left_knee": 0.5,
                  "right_knee": 0.5,
                  "back": 1.0,
                  "left_ankle": 0.0,
                  "right_ankle": 0.0,
                  "upper_back": 1.0,
                  "left_foot": 0.0,
                  "right_foot": 0.0,
                  "neck_low": 1.0,
                  "left_arm_sholder_upper": 1.0,
                  "right_arm_sholder_upper": 1.0,
                  "neck_high": 1.0,
                  "left_arm_sholder_low": 1.0,
                  "right_arm_sholder_low": 1.0,
                  "left_cubit": 1.0,
                  "right_cubit": 1.0,
                  "left_hand": 1.0,
                  "right_hand": 1.0,
                  "left_hand_fingers": 1.0,
                  "right_hand_fingers": 1.0}
    };
    return result;


def get_cloth_weight_matrix(smpl_model, skip_socks = False):
    feet_indices = [10, 11];
    knee_indices = [4, 5];
    ankle_indices = [7, 8];
    cubit_indices = [18, 19];
    hand_indices = [22, 23];
    carpus_indices = [20, 21];
    neck_indices = [12, 15];

    joints_mask = np.ones(24);

    if not skip_socks:
        joints_mask[feet_indices] = 0;
        joints_mask[ankle_indices] = 0;
    joints_mask[cubit_indices] = 0.5;
    joints_mask[hand_indices] = 0;
    joints_mask[carpus_indices] = 0.2;
    joints_mask[neck_indices] = 0.2;

    result = np.dot(smpl_model.weights, joints_mask);

    return result;

def get_women_cloth_weight_matrix(smpl_model, skip_socks = False):
    feet_indices = [10, 11];
    knee_indices = [4, 5];
    ankle_indices = [7, 8];
    cubit_indices = [18, 19];
    hand_indices = [22, 23];
    carpus_indices = [20, 21];
    neck_indices = [12, 15];

    joints_mask = np.ones(24);

    if not skip_socks:
        joints_mask[feet_indices] = 0.0;
        joints_mask[ankle_indices] = 0.0;
    joints_mask[knee_indices] = 0.2;
    joints_mask[cubit_indices] = 0.2;
    joints_mask[hand_indices] = 0;
    joints_mask[carpus_indices] = 0.0;
    joints_mask[neck_indices] = 0.2;

    result = np.dot(smpl_model.weights, joints_mask);

    return result;


def get_skin_weight_matrix(smpl_model):
    cloth_weights = get_cloth_weight_matrix(smpl_model);

    cloth_weights = cloth_weights * 1.0;

    cloth_weights_tensor = torch.tensor(cloth_weights);

    max_weight = torch.max(cloth_weights_tensor);

    skin_weights = max_weight - cloth_weights_tensor;

    return skin_weights;

def get_women_skin_weight_matrix(smpl_model):
    cloth_weights = get_women_cloth_weight_matrix(smpl_model);

    cloth_weights = cloth_weights * 1.0;

    cloth_weights_tensor = torch.tensor(cloth_weights);

    max_weight = torch.max(cloth_weights_tensor);

    skin_weights = max_weight - cloth_weights_tensor;

    return skin_weights;

def get_skin_without_socks_weight_matrix(smpl_model):
    cloth_weights = get_cloth_weight_matrix(smpl_model, True);

    cloth_weights = cloth_weights * 1.0;

    cloth_weights_tensor = torch.tensor(cloth_weights);

    max_weight = torch.max(cloth_weights_tensor);

    skin_weights = max_weight - cloth_weights_tensor;

    return skin_weights;

def get_women_skin_without_socks_weight_matrix(smpl_model):
    cloth_weights = get_women_cloth_weight_matrix(smpl_model, True);

    cloth_weights = cloth_weights * 1.0;

    cloth_weights_tensor = torch.tensor(cloth_weights);

    max_weight = torch.max(cloth_weights_tensor);

    skin_weights = max_weight - cloth_weights_tensor;

    return skin_weights;


def get_garmet_weight(smpl_model, garment_name):

    joints = joints_dict();
    anti_gament_dict = not_garmets_dict();
    garment_dict = garmets_dict();

    garment_joints_dict = anti_gament_dict[garment_name];

    joints_mask = np.zeros(24);

    vals = garment_joints_dict.values();
    keys = garment_joints_dict.keys();
    indices = [joints[x] for x in keys]

    joints_mask[indices] = list(vals);

    max_weight = np.max(np.array(smpl_model.weights));


    result = np.dot(smpl_model.weights, 1 - joints_mask);


    return result;

def get_no_cloth_weights(smpl_model, cloth_keys):
    joints = joints_dict();
    anti_gament_dict = not_garmets_dict();
    garment_dict = garmets_dict();


    joints_mask = [];

    for garment_name in cloth_keys:
        local_joints_mask = np.zeros(24);
        garment_joints_dict = anti_gament_dict[garment_name];
        vals = garment_joints_dict.values();
        keys = garment_joints_dict.keys();
        local_indices = [joints[x] for x in keys];
        local_joints_mask[local_indices] = list(vals);
        joints_mask.append(local_joints_mask)

    all_jounts_mask = np.sum(joints_mask, axis=0) / len(cloth_keys);

    max_weight = np.max(np.array(smpl_model.weights));

    result = np.dot(smpl_model.weights, all_jounts_mask);

    return result;
