import numpy as np
import scipy
import torch
from scipy.optimize import fmin_bfgs

from cloth_cap_stuff import Ec_adopt, single_mesh_C_grad_fn, smpl_part_loss, grad_fn_smpl, \
    grad_fn_Ec, grad_fn_by_pose_smpl
from math_utils import pair_wise_squared_dist_matrix_torch, get_connection_matrix_from_traingles, \
    find_cycles
from ply_utils import get_scan_corrds_from_ply_data, ply_data_set_colors, get_colors_from_ply_data, \
    get_triangles_from_ply_data
from plyfile import PlyData
from smpl_utils import save_smpl_to_obj, debug_safe_smpl_with_mesh
from smpl_webuser.serialization import save_model



def adjust_smpl_to_mesh_by_pose_n_shape_find_learning_rate(smpl_pose_array, smpl_shape_array,
                                                           pose_grad, shape_grad,
                                                           smpl_data, smpl_data_with_needed_shape,
                                                           coords_shape_tensor,
                                                           triangles_shape_tensor,
                                                           skin_weights, w_c, w_pose, w_shape, change_shape = True):

    loss = grad_fn_smpl(  torch.cat((smpl_shape_array, smpl_pose_array)).numpy(),
                            smpl_data,
                            smpl_data_with_needed_shape,
                            coords_shape_tensor,
                            triangles_shape_tensor,
                            skin_weights, w_c, w_pose, w_shape);
    lr = 0.01;
    prev_lr = lr;
    print('adjust_smpl_to_mesh_by_pose_n_shape_find_learning_rate initial loss = ', loss, ', initial learning rate =', lr,' \n');

    loss  = 50000000000000;
    prev_loss = loss;

    while loss <= prev_loss:
        prev_lr = lr;
        lr = lr / 2.0;
        current_smpl_pose_array = smpl_pose_array - torch.tensor(pose_grad * lr);
        if change_shape:
            current_smpl_shape_array = smpl_shape_array - torch.tensor(shape_grad * lr);
        else:
            current_smpl_shape_array = smpl_shape_array[:]
        prev_loss = loss;
        loss = grad_fn_smpl(torch.cat((current_smpl_shape_array, current_smpl_pose_array)).numpy(),
                            smpl_data,
                            smpl_data_with_needed_shape,
                            coords_shape_tensor,
                            triangles_shape_tensor,
                            skin_weights, w_c, w_pose, w_shape);

        print('adjust_smpl_to_mesh_by_pose_n_shape_find_learning_rate loss = ', loss, ', learning rate =', lr, ' \n');

    return prev_lr;


def adjust_smpl_to_mesh_by_pose_shape(init_smpl_model,
                                      coords_shape_tensor,
                                      triangles_shape_tensor,
                                      smpl_data_with_needed_shape,
                                      gradient_step,
                                      skin_weights, w_c, w_pose, w_shape,
                                      threshold,
                                      max_iterations,
                                      change_shape = True,
                                      debug_folder = None,
                                      debug_file_preffix = None):
    smpl_data = init_smpl_model;
    smpl_shape_array = torch.tensor(np.array(smpl_data.betas));
    smpl_pose_array = torch.tensor(np.array(smpl_data.pose));


    loss = smpl_part_loss(smpl_data, smpl_data_with_needed_shape,
                          coords_shape_tensor,
                          triangles_shape_tensor,
                          skin_weights, w_c, w_pose, w_shape);
    print('adjust_smpl adjust_smpl_to_mesh_by_pose_shape = ', loss, ' \n');

    prev_loss = loss;
    counter = 0;

    if debug_folder is not None:
        debug_file_path = debug_folder + debug_file_preffix + str(counter) + r'.obj';
        save_smpl_to_obj(smpl_data, debug_file_path);

    while ((prev_loss - loss >= threshold or (counter == 0)) and counter <= max_iterations):

        grad_smpl_part = scipy.optimize.approx_fprime(torch.cat((smpl_shape_array,
                                                                 smpl_pose_array)).numpy(),
                                                      grad_fn_smpl,
                                                      gradient_step,
                                                      smpl_data,
                                                      smpl_data_with_needed_shape,
                                                      coords_shape_tensor,
                                                      triangles_shape_tensor,
                                                      skin_weights,
                                                      w_c, w_pose, w_shape);

        shape_grad = grad_smpl_part[0: smpl_data.betas.shape[0]];
        pose_grad = grad_smpl_part[smpl_data.betas.shape[0]:];

        lr = adjust_smpl_to_mesh_by_pose_n_shape_find_learning_rate(   smpl_pose_array, smpl_shape_array,
                                                                       pose_grad, shape_grad,
                                                                       smpl_data, smpl_data_with_needed_shape,
                                                                       coords_shape_tensor,
                                                                       triangles_shape_tensor,
                                                                       skin_weights, w_c, w_pose, w_shape, change_shape);
        if change_shape:
            smpl_shape_array[:] = smpl_shape_array - torch.tensor(shape_grad * lr);

        smpl_pose_array[:] = smpl_pose_array - torch.tensor(pose_grad * lr);

        prev_loss = loss;
        loss = grad_fn_smpl(torch.cat((smpl_shape_array, smpl_pose_array)).numpy(),
                            smpl_data,
                            smpl_data_with_needed_shape,
                            coords_shape_tensor,
                            triangles_shape_tensor,
                            skin_weights, w_c, w_pose, w_shape);
        print('adjust_smpl adjust_smpl_to_mesh_by_pose_shape = ', loss, ' \n');
        counter += 1;

        if debug_folder is not None:
            smpl_shape_array_old = torch.tensor(np.array(smpl_data.betas));
            smpl_pose_array_old = torch.tensor(np.array(smpl_data.pose));
            smpl_data.betas[:] = smpl_shape_array[:]
            smpl_data.pose[:] = smpl_pose_array[:]
            debug_file_path = debug_folder + debug_file_preffix + str(counter) + r'.obj';
            save_smpl_to_obj(smpl_data, debug_file_path);
            smpl_data.betas[:] = smpl_shape_array_old;
            smpl_data.pose[:] = smpl_pose_array_old;

        if prev_loss > loss:
            smpl_data.betas[:] = smpl_shape_array[:]
            smpl_data.pose[:] = smpl_pose_array[:]

    if debug_folder is not None:
        debug_file_path = debug_folder + debug_file_preffix + str(counter) + r'.pkl';
        save_model(smpl_data, debug_file_path);

    return smpl_data;

def adjust_smpl_to_mesh_by_pose_find_learning_rate(smpl_pose_array, pose_grad,
                                                   smpl_data, coords_shape_tensor,
                                                   triangles_shape_tensor,
                                                   skin_weights, w_c, w_pose):

    loss = grad_fn_by_pose_smpl(  smpl_pose_array, smpl_data,
                                  coords_shape_tensor,
                                  triangles_shape_tensor,
                                  skin_weights, w_c, w_pose);
    lr = 0.01;
    prev_lr = lr;
    print('adjust_smpl_to_mesh_by_pose_find_learning_rate initial loss = ', loss, ', initial learning rate =', lr,' \n');

    loss  = 50000000000000;
    prev_loss = loss;

    while loss <= prev_loss:
        prev_lr = lr;
        lr = lr / 2.0;
        current_smpl_pose_array = smpl_pose_array - np.array(pose_grad * lr);
        prev_loss = loss;
        loss =  grad_fn_by_pose_smpl(current_smpl_pose_array, smpl_data,
                                     coords_shape_tensor,
                                     triangles_shape_tensor,
                                     skin_weights, w_c, w_pose);

        print('adjust_smpl_to_mesh_by_pose_find_learning_rate loss = ', loss, ', learning rate =', lr, ' \n');

    return prev_lr;

def adjust_smpl_to_mesh_find_learning_rate(initial_smpl_coords_array, smpl_coords_grad,
                                           smpl_data, posed_coords, v, momentum):

    loss = single_mesh_C_grad_fn(initial_smpl_coords_array, smpl_data, posed_coords);
    lr = 1.0;
    prev_lr = lr;
    print('adjust_smpl_to_mesh_find_learning_rate initial loss = ', loss, ', initial learning rate =', lr,' \n');

    loss  = 50000000;
    prev_loss = loss;

    if v is None:
        v = torch.zeros_like(smpl_coords_grad);

    while loss <= prev_loss:
        prev_lr = lr;
        lr = lr / 2.0;


        current_v = momentum * v - (smpl_coords_grad * lr);
        smpl_coords_array = initial_smpl_coords_array + current_v;

        #smpl_coords_array = initial_smpl_coords_array - np.array(smpl_coords_grad * lr);
        prev_loss = loss;
        loss = single_mesh_C_grad_fn(smpl_coords_array, smpl_data, posed_coords);
        print('adjust_smpl_to_mesh_find_learning_rate loss = ', loss, ', learning rate =', lr, ' \n');

    return prev_lr;

def callback(xk, state=None):
    print(xk);
    if state is not None:
        print(state)

def adjust_smpl_to_mesh(posed_smpl_model, posed_coords, gradient_step, initial_lr, threshold,
                        debug_folder_path = None):

    smpl_data = posed_smpl_model;

    smpl_pose_array_old = np.array(smpl_data.pose[:]);

    smpl_data.pose[:] = 0;

    smpl_coords_array = np.array(smpl_data.v_template[:]).flatten();

    smpl_data.pose[:] = smpl_pose_array_old[:];

    loss = single_mesh_C_grad_fn(smpl_coords_array, smpl_data, posed_coords);

    prev_loss = loss;
    print('adjust_smpl_to_mesh loss = ', loss, ' \n');
    counter = 0;
    if debug_folder_path is not None:
        debug_safe_smpl_with_mesh(smpl_data, smpl_coords_array, debug_folder_path, counter);

    keep_doing = True;

    lr = initial_lr;
    print('adjust_smpl_to_mesh lr = ', lr, ' \n');

#    coords = scipy.optimize.minimize(single_mesh_C_grad_fn,
#                                     smpl_coords_array,
#                                     args=(smpl_data, posed_coords),
#                                     method='Powell',
#                                     callback=callback);

    coords = old_minimizing(counter, debug_folder_path, gradient_step, keep_doing, loss, posed_coords, smpl_coords_array,
                   smpl_data, threshold, 0.5)


    N = smpl_data.r.shape[0];

    mesh_coords_to_use = np.reshape(coords, (N, 3));

    smpl_data.v_template[:] = mesh_coords_to_use[:];
    smpl_data.pose[:] = smpl_pose_array_old;

    return smpl_data;


def old_minimizing(counter, debug_folder_path, gradient_step, keep_doing, loss, posed_coords, smpl_coords_array,
                   smpl_data, threshold, momentum):
    grad = None;
    v = None;
    while keep_doing:

        if grad is not None:
            prev_grad = grad;

        grad = scipy.optimize.approx_fprime(smpl_coords_array,
                                            single_mesh_C_grad_fn,
                                            gradient_step,
                                            smpl_data,
                                            posed_coords);

        if v is None:
            v = np.zeros_like(grad);



        lr = adjust_smpl_to_mesh_find_learning_rate(smpl_coords_array, grad,
                                                    smpl_data, posed_coords, v, momentum);

        v = momentum * v - (grad * lr);
        smpl_coords_array[:] = smpl_coords_array[:] + v;

        #smpl_coords_array[:] = smpl_coords_array - np.array(grad * lr);
        prev_loss = loss;
        loss = single_mesh_C_grad_fn(smpl_coords_array, smpl_data, posed_coords);
        print('adjust_smpl_to_mesh loss = ', loss, ' \n');
        counter += 1;
        if debug_folder_path is not None:
            debug_safe_smpl_with_mesh(smpl_data, smpl_coords_array, debug_folder_path, counter);

        keep_doing = ((prev_loss - loss) > threshold);
    return  smpl_coords_array;


def get_k_nearest_points_from_scan(ply_data_mesh, scaled_scan_coords_tensor, k):
    deformed_template_coords_tensor = get_scan_corrds_from_ply_data(ply_data_mesh)

    squared_distance_map = pair_wise_squared_dist_matrix_torch(scaled_scan_coords_tensor,
                                                               deformed_template_coords_tensor);

    nearest = torch.topk(squared_distance_map, k, dim=0, largest=False);

    return nearest.indices.t();

def get_garment_rings(segmented_ply_path,
                      segmented_border_template_path = None,
                      debug_folder = None,
                      debug_file_preffix = None):
    segemented_ply_data = PlyData.read(segmented_ply_path);

    segemented_ply_colors = get_colors_from_ply_data(segemented_ply_data);
    segmented_ply_triangles = get_triangles_from_ply_data(segemented_ply_data);

    vertex_count = segemented_ply_colors.shape[0];
    colors_as_numbers = segemented_ply_colors.long()[:, 0] * 255 * 255 + \
                        segemented_ply_colors.long()[:, 1] * 255 + \
                        segemented_ply_colors.long()[:, 2];

    connection_matrix = get_connection_matrix_from_traingles(vertex_count,
                                                             segmented_ply_triangles);

    label_rows = colors_as_numbers.unsqueeze(dim=1).repeat((1, vertex_count));
    label_cols = colors_as_numbers.unsqueeze(dim=0).repeat((vertex_count, 1));

    border_matrix = (label_rows != label_cols).int() * connection_matrix;

    indices_left = np.where(border_matrix != 0);

    segemented_ply_colors[indices_left[0], :] = 255;

    segemented_ply_data = ply_data_set_colors(segemented_ply_data, segemented_ply_colors);

    if segmented_border_template_path is not None:
        segemented_ply_data.write(segmented_border_template_path)

    rings = [];

    counter = 0;

    while border_matrix.sum() > 0:
        indices_left = np.where(border_matrix != 0);
        first_color = colors_as_numbers[indices_left[0][0]];
        second_color = colors_as_numbers[indices_left[1][0]];
        local_matrix_first_color = (label_rows == first_color).int() * connection_matrix;

        local_border_matrix_diff = (label_rows != label_cols).int() * connection_matrix;
        local_border_matrix = local_matrix_first_color * \
                              local_border_matrix_diff;

        border_indices = np.vstack(np.where(local_border_matrix != 0));

        matrix_for_work = torch.zeros_like(connection_matrix);

        matrix_for_work[border_indices[0, :], :] = connection_matrix[border_indices[0, :], :];
        matrix_for_work[:, border_indices[0, :]] = connection_matrix[:, border_indices[0, :]];

        matrix_for_work = matrix_for_work * (1 - local_border_matrix);

        garment_rings = find_cycles(matrix_for_work);

        for garment_ring in garment_rings:
            if len(garment_ring) < 10:
                garment_rings.remove(garment_ring);

        border_matrix = border_matrix * (1 - local_matrix_first_color);

        save_garment_ring_to_ply_file(counter, debug_file_preffix, debug_folder, garment_rings,
                                      segmented_ply_path)

        counter += 1;

        rings.append(garment_rings);

    return rings;


def save_garment_ring_to_ply_file(counter, debug_file_preffix, debug_folder, garment_rings, segmented_ply_path):

    segemented_ply_data_copy = PlyData.read(segmented_ply_path);
    segemented_ply_colors_copy = get_colors_from_ply_data(segemented_ply_data_copy);

    i = 0;
    step = 255 / len(garment_rings);
    for ring in garment_rings:
        segemented_ply_colors_copy[ring, 0] = step * (i + 1);
        segemented_ply_colors_copy[ring, 1] = 255;
        segemented_ply_colors_copy[ring, 2] = 255 - step * (i + 1);
        i += 1;
    segemented_ply_data_copy = ply_data_set_colors(segemented_ply_data_copy,
                                                   segemented_ply_colors_copy);
    if debug_folder is not None and debug_file_preffix is not None:
        segemented_ply_data_copy.write(debug_folder + debug_file_preffix + str(counter) + '.ply');
