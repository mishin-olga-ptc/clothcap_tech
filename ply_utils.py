from array import array

import numpy as np
import torch
from skimage.color import rgb2hsv
from sklearn.cluster import KMeans

from body_shape_weights import get_cloth_weight_matrix
from math_utils import scale_scan_coods, get_mass_center_from_indices
from plyfile import PlyData
from smpl_utils import get_A_pose_params, save_smpl_to_obj
from smpl_webuser.serialization import load_model


def get_colors_from_ply_data(ply_data):

    ply_memmap = ply_data.elements[0].data[:];

    reds = ply_memmap[:]['red'];
    greens = ply_memmap[:]['green'];
    blues = ply_memmap[:]['blue'];

    colors_tensor = torch.tensor([reds, greens, blues]).t();

    return colors_tensor;


def get_scan_corrds_from_ply_data(ply_data):

    scan_coords_memmap = ply_data.elements[0].data[:];

 #   if (len(ply_data.elements[0].properties) == 9):
 #       xs = scan_coords_memmap[:]['nx'];
 #       ys = scan_coords_memmap[:]['ny'];
 #       zs = scan_coords_memmap[:]['nz'];
 #   else:

    xs = scan_coords_memmap[:]['x'];
    ys = scan_coords_memmap[:]['y'];
    zs = scan_coords_memmap[:]['z'];

    scan_coords_tensor = torch.tensor([xs, ys, zs]).t();

    return scan_coords_tensor;


def get_triangles_from_ply_data(ply_data):

    tri_data = ply_data['face'].data['vertex_indices']
    triangles = np.vstack(tri_data)

    return triangles;



def set_scan_corrds_from_ply_data(ply_data, scan_coords_tensor):

    scan_coords_memmap = ply_data.elements[0].data[:];

    xs = scan_coords_tensor[:, 0];
    ys = scan_coords_tensor[:, 1];
    zs = scan_coords_tensor[:, 2];

    scan_coords_memmap[:]['x'] = xs;
    scan_coords_memmap[:]['y'] = ys;
    scan_coords_memmap[:]['z'] = zs;

    return ply_data;



def debug_safe_ply(basic_ply_path, deformed_template_coords_copy, debug_file_path):

    basic_pose_ply_data = PlyData.read(basic_ply_path);

    set_scan_corrds_from_ply_data(basic_pose_ply_data, deformed_template_coords_copy.clone().detach());

    basic_pose_ply_data.write(debug_file_path);

def color_ply_from_weights(ply_data, base_ply_data, weights):
    vetrices = ply_data.elements[0]
    scan_coords_memmap = ply_data.elements[0].data[:];
    new_coords = get_scan_corrds_from_ply_data(base_ply_data);
    colors = np.zeros((weights.shape[0], 3), dtype=np.ubyte);
    max_weight = np.max(weights);
    min_weight = np.min(weights);
    range = max_weight - min_weight;

    n = weights.shape[0];
    n_faces = base_ply_data.elements[1].data.shape[0];

    colors[:, 0] = (weights - min_weight) * 255 / range;
    colors[:, 2] = 255;

    xs = new_coords[:, 0];
    ys = new_coords[:, 1];
    zs = new_coords[:, 2];

    new_coords_nd_array = new_coords.numpy();

    new_data = np.concatenate((new_coords_nd_array, colors), axis=1);

    ply_data.elements[0].data[:] = 0;
    ply_data.elements[1].data[:] = 0;
    ply_data.elements[1].data[0:n_faces] = base_ply_data.elements[1].data[:];

    ply_data.elements[0].data[0 : n][ 'x'] = xs;
    ply_data.elements[0].data[0 : n][ 'y'] = ys;
    ply_data.elements[0].data[0 : n][ 'z'] = zs;
    ply_data.elements[0].data[0 : n][ 'red'] = colors[:, 0];
    ply_data.elements[0].data[0 : n][ 'green'] = colors[:, 1];
    ply_data.elements[0].data[0 : n][ 'blue'] = colors[:, 2];

    ply_data.elements[0].data = ply_data.elements[0].data[:n];
    ply_data.elements[1].data = ply_data.elements[1].data[:n_faces];

    return ply_data;


def ply_data_set_colors(ply_data, colors):

    ply_data.elements[0].data[:][ 'red'] = colors[:, 0];
    ply_data.elements[0].data[:][ 'green'] = colors[:, 1];
    ply_data.elements[0].data[:][ 'blue'] = colors[:, 2];

    return ply_data;

def color_ply_from_clusters(ply_data, base_ply_data, clusters):
    vetrices = ply_data.elements[0]
    scan_coords_memmap = ply_data.elements[0].data[:];
    new_coords = get_scan_corrds_from_ply_data(base_ply_data);
    colors = np.zeros((clusters.shape[0], 3), dtype=np.ubyte);

    n = clusters.shape[0];
    n_faces = base_ply_data.elements[1].data.shape[0];

    n_colors = np.max(clusters) + 1;
    colors_steps = n_colors / 3 + 1;

    if n_colors < 3:
        colors[np.where(clusters == 0)[0], 0] = 255;
        colors[np.where(clusters == 0)[0], 1] = 0;
        colors[np.where(clusters == 0)[0], 2] = 0;
        colors[np.where(clusters == 1)[0], 0] = 0;
        colors[np.where(clusters == 1)[0], 1] = 255;
        colors[np.where(clusters == 1)[0], 2] = 0;
        colors[np.where(clusters == 2)[0], 0] = 0;
        colors[np.where(clusters == 2)[0], 1] = 0;
        colors[np.where(clusters == 2)[0], 2] = 255;
    else:
        colors[:, 0] = clusters / n_colors * 255;
        colors[:, 1] = clusters / n_colors * 255;
        colors[:, 2] = 255;

    xs = new_coords[:, 0];
    ys = new_coords[:, 1];
    zs = new_coords[:, 2];

    new_coords_nd_array = new_coords.numpy();

    new_data = np.concatenate((new_coords_nd_array, colors), axis=1);

    ply_data.elements[0].data[:] = 0;
    ply_data.elements[1].data[:] = 0;
    ply_data.elements[1].data[0:n_faces] = base_ply_data.elements[1].data[:];

    ply_data.elements[0].data[0 : n][ 'x'] = xs;
    ply_data.elements[0].data[0 : n][ 'y'] = ys;
    ply_data.elements[0].data[0 : n][ 'z'] = zs;
    ply_data.elements[0].data[0 : n][ 'red'] = colors[:, 0];
    ply_data.elements[0].data[0 : n][ 'green'] = colors[:, 1];
    ply_data.elements[0].data[0 : n][ 'blue'] = colors[:, 2];

    ply_data.elements[0].data = ply_data.elements[0].data[:n];
    ply_data.elements[1].data = ply_data.elements[1].data[:n_faces];

    return ply_data;

def crop_ply_data_by_color(input_ply_data, output_ply_data, color_to_keep):
    vetrices = output_ply_data.elements[0]
    scan_coords_memmap = output_ply_data.elements[0].data[:];

    red = color_to_keep[0];
    green = color_to_keep[1];
    blue = color_to_keep[2];

    colors_red = output_ply_data.elements[0].data[:][ 'red']
    colors_green = output_ply_data.elements[0].data[:][ 'green']
    colors_blue = output_ply_data.elements[0].data[:][ 'blue']

    vertex_indices_to_remove = np.where((colors_red == red) * (colors_green == green) * (colors_blue == colors_blue) == 0);

    faces_count = output_ply_data.elements[1].data.shape[0];

    tri_data = output_ply_data['face'].data['vertex_indices']
    triangles = np.vstack(tri_data)

    triangles_indices_to_remove = np.argwhere(np.isin(triangles, vertex_indices_to_remove))[:, 0]

    triangles[triangles_indices_to_remove, :] = 0;

    scan_coords_memmap = output_ply_data.elements[0].data[:];

    xs = scan_coords_memmap[:]['x'];
    ys = scan_coords_memmap[:]['y'];
    zs = scan_coords_memmap[:]['z'];

    xs[vertex_indices_to_remove] = 0.0;
    ys[vertex_indices_to_remove] = 0.0;
    zs[vertex_indices_to_remove] = 0.0;

    scan_coords_memmap[:]['x'] = xs;
    scan_coords_memmap[:]['y'] = ys;
    scan_coords_memmap[:]['z'] = zs;


    for i in range(0, faces_count):
        tri_data[i] = triangles[i, :];


    return output_ply_data;



def cluster_ply_scan(ply_data, num_clusters):
    rgb_colors = get_colors_from_ply_data(ply_data);
    hsv_colors = torch.tensor(rgb2hsv(rgb_colors.unsqueeze(dim = 0))).squeeze();

    kmeans_data = KMeans(n_clusters=num_clusters, random_state=3).fit(hsv_colors);

    return kmeans_data.labels_;


def save_ply_colored_by_weights(scan_ply_path, base_ply_data, weights, file_path):
    scan_ply_data_copy = PlyData.read(scan_ply_path);
    ply_test_data = color_ply_from_weights(scan_ply_data_copy, base_ply_data, weights);
    ply_test_data.write(file_path);

def scale_n_save_ply_colored_by_clusters(scan_ply_path, base_ply_data, coords_to_scale_to, new_coords,
                                         clusters, file_path,
                                         height_scale_factor = None,
                                         custom_height_factor=1.0,
                                         backward_offset = 0.0,
                                         move_mask = None):
    scan_ply_data_copy = PlyData.read(scan_ply_path);
    ply_test_data = color_ply_from_clusters(scan_ply_data_copy, base_ply_data, clusters);
  #  coords = get_scan_corrds_from_ply_data(ply_test_data);
  #  new_coords = scale_scan_coods(coords, coords_to_scale_to,
  #                                height_scale_factor=height_scale_factor,
  #                                custom_height_factor=custom_height_factor,
  #                                backward_offset=backward_offset,
  #                                move_mask=move_mask);
    ply_data_to_save = set_scan_corrds_from_ply_data(ply_test_data, new_coords);
    ply_data_to_save.write(file_path);



def get_ply_mass_center_of_indices(ply_data, indices):
    coords = get_scan_corrds_from_ply_data(ply_data);

    result = get_mass_center_from_indices(coords, indices);

    return result;


def get_ply_mass_center_from_weights(ply_data, weights):
    coords = get_scan_corrds_from_ply_data(ply_data);

    result = get_mass_center_from_indices(coords.numpy(), weights);

    return result;


def split_ply_file_by_its_colors(input_ply_path, output_directory, output_file_preffix):
    input_ply_data = PlyData.read(input_ply_path);
    colors = get_colors_from_ply_data(input_ply_data);
    unique_colors = np.unique(colors, axis=0);

    for color in unique_colors:
        input_ply_data = PlyData.read(input_ply_path);
        output_ply_data = PlyData.read(input_ply_path);
        output_path = output_directory + \
                      output_file_preffix + \
                      str(color[0]) + '_' + str(color[1]) + '_' + str(color[2]) + \
                      '.ply';
        output_ply_data = crop_ply_data_by_color(output_ply_data, input_ply_data, color);
        output_ply_data.write(output_path);




